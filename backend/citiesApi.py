# source env/bin/activate before can run python stuff

import requests
import pprint
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import json
import sys
from apiModels import Profession, City, JobCount, app, db


api_key = "zplkyjepwzramj&"

# dict to map city name to its unique city id, id not currently used but could be useful in the future
cities = dict()

# retrieves the list of all the cities available from the API
def getCities():
    url = "https://www.numbeo.com/api/cities?api_key=" + api_key
    payload = {"Content-Type": "application/json"}
    response = requests.get(url=url)
    return json.loads(response.text)


# parses the list of cities available and puts them into the cities dict
def parseCities():
    # get raw json from the api call
    json = getCities()
    citiesArray = json["cities"]
    # only consider cities in the US
    for cityObject in citiesArray:
        if cityObject["country"] == "United States":
            cityName = cityObject["city"]
            cityId = cityObject["city_id"]
            cities[cityName] = cityId
            # print(str(cityName) + str(cityId))


# updates the database with information about a specific city
def updateCityInformation(cityName):
    url = (
        "https://www.numbeo.com/api/indices?api_key="
        + api_key
        + "city_id="
        + str(cities[cityName])
    )
    payload = {"Content-Type": "application/json"}
    response = requests.get(url=url)
    jsonObj = json.loads(response.text)

    try:
        # information about a city that we care about
        crime_index = jsonObj["crime_index"]
        rent_index = jsonObj["rent_index"]
        health_care_index = jsonObj["health_care_index"]
        pollution_index = jsonObj["pollution_index"]
        climate_index = jsonObj["climate_index"]

        # check if this entry already exists
        exists = City.query.filter_by(city_name=cityName).scalar() is not None
        # add only if it doesn't already exist and all of the fields are valid
        if not exists:
            # print("city name: " + cityName + " does not exist")
            if None not in (
                crime_index,
                rent_index,
                health_care_index,
                pollution_index,
                climate_index,
            ):
                # print(str(cityName) + "crime_index: " + str(crime_index) + " " + "rent_index: " + str(rent_index) + " " + "health_care_index: " + str(health_care_index) + " " + "pollution_index: " + str(pollution_index) + " " + "climate_index: " + str(climate_index))
                db.session.add(
                    City(
                        city_name=cityName,
                        crime_index=crime_index,
                        rent_index=rent_index,
                        health_care_index=health_care_index,
                        pollution_index=pollution_index,
                        climate_index=climate_index,
                    )
                )
                db.session.commit()

    except KeyError:
        pass


db.create_all()
# get the list of available cities from the API
parseCities()

# counter = 0
# for each of those cities, get its information and add it to the database
for cityName in cities:
    updateCityInformation(cityName)

    # counter += 1
    # if counter % 50 == 0:
    # print("counter is " + str(counter))

# print("Done updating city information!!!")
