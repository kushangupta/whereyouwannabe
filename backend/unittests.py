import json
from unittest import main, TestCase, mock
from unittest.mock import MagicMock
from sqlalchemy.orm import sessionmaker
from application import convert_to_dict, response, get_cities, run_everything
from apiModels import engine, City, Profession, University, db, JobCount, app


class UnitTests(TestCase):
    def test_response(self):
        d = {"key": "value", "Number": 1}
        result = response(d)
        stringify = str(result.data, "utf-8")
        status = result.status
        self.assertIn('"key": "value"', stringify)
        self.assertIn('"Number": 1', stringify)
        self.assertEqual("200 OK", status)

    def test_convert_to_dict(self):
        prof = Profession(
            id=1,
            profession_name="Test",
            profession_median_wage=50000,
            profession_description="description",
            profession_education="None",
            profession_outlook="Bad",
            profession_video=None,
            profession_projection=5,
        )
        l = [prof]
        results = convert_to_dict(l)
        d = results[0]
        self.assertIsInstance(d, dict)
        self.assertEqual(d["id"], 1)
        self.assertEqual(d["profession_name"], "Test")
        self.assertEqual(d["profession_median_wage"], 50000)
        self.assertEqual(d["profession_description"], "description")
        self.assertEqual(d["profession_education"], "None")
        self.assertEqual(d["profession_outlook"], "Bad")
        self.assertIsNone(d["profession_video"])
        self.assertEqual(d["profession_projection"], 5)

    def test_create_profession(self):
        run_everything()

        try:
            professionResult = (
                session.query(Profession)
                .filter_by(profession_name="test profession name")
                .first()
            )
            session.delete(professionResult)
            session.commit()
        except:
            pass

        fakeProfession = Profession(
            id=-1,
            profession_name="test profession name",
            profession_median_wage=-1,
            profession_description="test profession description",
            profession_education="test profession education",
            profession_outlook="test profession outlook",
            profession_video="test profession video",
            profession_projection=1,
            profession_photo="test profession photo",
            profession_major="test profession major",
        )

        try:
            session.add(fakeProfession)
            session.commit()

            professionResult = (
                session.query(Profession)
                .filter_by(profession_name="test profession name")
                .first()
            )
            self.assertEqual(
                str(professionResult.profession_name), "test profession name"
            )

            session.delete(professionResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_create_profession")

            session.rollback()
            assert False

    def test_create_city(self):
        run_everything()

        try:
            cityResult = (
                session.query(City).filter_by(city_name="test city name").first()
            )
            session.delete(cityResult)
            session.commit()
        except:
            pass

        fakeCity = City(
            id=-2,
            city_name="test city name",
            crime_index=-1,
            rent_index=-1,
            health_care_index=-1,
            pollution_index=-1,
            climate_index=-1,
            photo="test photo",
        )

        try:
            session.add(fakeCity)
            session.commit()

            cityResult = (
                session.query(City).filter_by(city_name="test city name").first()
            )
            self.assertEqual(str(cityResult.city_name), "test city name")

            session.delete(cityResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_create_city")

            session.rollback()
            assert False

    def test_create_job_count(self):
        run_everything()

        try:
            jobCountResult = (
                session.query(JobCount).filter_by(profession="test profession").first()
            )
            session.delete(jobCountResult)
            session.commit()
        except:
            pass

        fakeJobCount = JobCount(
            id=-1,
            profession="test profession",
            city_name="test city name",
            job_count=999,
        )

        try:
            session.add(fakeJobCount)
            session.commit()

            jobCountResult = (
                session.query(JobCount).filter_by(profession="test profession").first()
            )
            self.assertEqual(str(jobCountResult.profession), "test profession")

            session.delete(jobCountResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_create_job_count")

            session.rollback()
            assert False

    def test_create_university(self):
        run_everything()

        try:
            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )
            session.delete(universityResult)
            session.commit()
        except:
            pass

        fakeUniversity = University(
            id=-1,
            university_name="test university name",
            city_name="test city name",
            state_name="test state name",
            student_size=-1,
            admission_rate=-1,
            academic_year_cost=-1,
            demographics_women=-1,
            demographics_men=-1,
            demographics_white=-1,
            demographics_black=-1,
            demographics_hispanic=-1,
            demographics_asian=-1,
            top_major="test top major",
            photo="test photo",
        )

        try:
            session.add(fakeUniversity)
            session.commit()

            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )
            self.assertEqual(
                str(universityResult.university_name), "test university name"
            )

            session.delete(universityResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_create_university")

            session.rollback()
            assert False

    def test_create_multiple(self):
        run_everything()

        try:
            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )
            jobCountResult = (
                session.query(JobCount).filter_by(profession="test profession").first()
            )
            cityResult = (
                session.query(City).filter_by(city_name="test city name").first()
            )

            session.delete(universityResult)
            session.delete(jobCountResult)
            session.delete(cityResult)
            session.commit()
        except:
            pass

        fakeJobCount = JobCount(
            id=-2,
            profession="test profession",
            city_name="test city name",
            job_count=999,
        )

        fakeCity = City(
            id=-2,
            city_name="test city name",
            crime_index=-1,
            rent_index=-1,
            health_care_index=-1,
            pollution_index=-1,
            climate_index=-1,
            photo="test photo",
        )

        fakeUniversity = University(
            id=-2,
            university_name="test university name",
            city_name="test city name",
            state_name="test state name",
            student_size=-1,
            admission_rate=-1,
            academic_year_cost=-1,
            demographics_women=-1,
            demographics_men=-1,
            demographics_white=-1,
            demographics_black=-1,
            demographics_hispanic=-1,
            demographics_asian=-1,
            top_major="test top major",
            photo="test photo",
        )

        try:
            session.add(fakeUniversity)
            session.add(fakeCity)
            session.add(fakeJobCount)
            session.commit()

            jobCountResult = (
                session.query(JobCount).filter_by(profession="test profession").first()
            )
            cityResult = (
                session.query(City).filter_by(city_name="test city name").first()
            )
            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )
            self.assertEqual(str(jobCountResult.profession), "test profession")
            self.assertEqual(str(cityResult.city_name), "test city name")
            self.assertEqual(
                str(universityResult.university_name), "test university name"
            )

            session.delete(jobCountResult)
            session.delete(cityResult)
            session.delete(universityResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_create_multiple")

            session.rollback()
            assert False

    def test_get_cities(self):
        run_everything()
        # remove any potential leftover info from previous test
        try:
            jobCountResult = (
                session.query(JobCount).filter_by(profession="test profession").first()
            )
            cityResult = (
                session.query(City).filter_by(city_name="test city name").first()
            )
            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )

            session.delete(jobCountResult)
            session.delete(cityResult)
            session.delete(universityResult)
            session.commit()
        except:
            pass

        fakeJobCount = JobCount(
            id=-2,
            profession="test profession",
            city_name="test city name",
            job_count=999,
        )

        fakeCity = City(
            id=-2,
            city_name="test city name",
            crime_index=-1,
            rent_index=-1,
            health_care_index=-1,
            pollution_index=-1,
            climate_index=-1,
            photo="test photo",
        )

        fakeUniversity = University(
            id=-2,
            university_name="test university name",
            city_name="test city name",
            state_name="test state name",
            student_size=-1,
            admission_rate=-1,
            academic_year_cost=-1,
            demographics_women=-1,
            demographics_men=-1,
            demographics_white=-1,
            demographics_black=-1,
            demographics_hispanic=-1,
            demographics_asian=-1,
            top_major="test top major",
            photo="test photo",
        )

        try:
            session.add(fakeUniversity)
            session.add(fakeCity)
            session.add(fakeJobCount)
            session.commit()

            jobCountResult = (
                session.query(JobCount).filter_by(profession="test profession").first()
            )
            cityResult = (
                session.query(City).filter_by(city_name="test city name").first()
            )
            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )
            self.assertEqual(str(jobCountResult.profession), "test profession")

            with app.test_client() as client:
                cities = client.get("/cities?city_name=test%20city%20name")
                citiesData = cities.data.decode("UTF-8")
                self.assertTrue('''"demographics_white": -1.0''' in citiesData)

            session.delete(jobCountResult)
            session.delete(cityResult)
            session.delete(universityResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_get_cities")

            session.rollback()
            assert False

    def test_get_professions(self):
        try:
            professionResult = (
                session.query(Profession)
                .filter_by(profession_name="test profession name")
                .first()
            )
            session.delete(professionResult)
            session.commit()
        except:
            pass

        run_everything()

        fakeProfession = Profession(
            id=-1,
            profession_name="test profession name",
            profession_median_wage=-1,
            profession_description="test profession description",
            profession_education="test profession education",
            profession_outlook="test profession outlook",
            profession_video="test profession video",
            profession_projection=1,
            profession_photo="test profession photo",
            profession_major="test profession major",
        )

        try:
            session.add(fakeProfession)
            session.commit()

            professionResult = (
                session.query(Profession)
                .filter_by(profession_name="test profession name")
                .first()
            )
            self.assertEqual(
                str(professionResult.profession_name), "test profession name"
            )

            with app.test_client() as client:
                professions = client.get(
                    "/professions?profession_name=test%20profession%20name"
                )
                professionsData = professions.data.decode("UTF-8")
                self.assertTrue(
                    '"profession_photo": "test profession photo"' in professionsData
                )

            session.delete(professionResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_get_professions")

            session.rollback()
            assert False

    def test_get_universities(self):
        run_everything()

        try:
            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )
            session.delete(universityResult)
            session.commit()
        except:
            pass

        fakeUniversity = University(
            id=-1,
            university_name="test university name",
            city_name="test city name",
            state_name="test state name",
            student_size=-1,
            admission_rate=-1,
            academic_year_cost=-1,
            demographics_women=-1,
            demographics_men=-1,
            demographics_white=-1,
            demographics_black=-1,
            demographics_hispanic=-1,
            demographics_asian=-1,
            top_major="test top major",
            photo="test photo",
        )

        try:
            session.add(fakeUniversity)
            session.commit()

            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )
            self.assertEqual(
                str(universityResult.university_name), "test university name"
            )

            with app.test_client() as client:
                universities = client.get(
                    "/universities?university_name=test%20university%20name"
                )
                universitiesData = universities.data.decode("UTF-8")
                # print("Universities data is: " + str(universitiesData))
                # self.assertTrue('''demographics_women''' in universitiesData)
                self.assertTrue('''"demographics_women": -1.0''' in universitiesData)

            session.delete(universityResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_get_professions")

            session.rollback()
            assert False

    def test_get_cities_filtered(self):
        run_everything()
        # remove any potential leftover info from previous test
        try:
            jobCountResult = (
                session.query(JobCount).filter_by(profession="test profession").first()
            )
            cityResult = (
                session.query(City).filter_by(city_name="testCity, testState").first()
            )
            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )

            session.delete(jobCountResult)
            session.delete(cityResult)
            session.delete(universityResult)
            session.commit()
        except:
            pass

        fakeJobCount = JobCount(
            id=-2,
            profession="test profession",
            city_name="testCity, testState",
            job_count=999,
        )

        fakeCity = City(
            id=-2,
            city_name="testCity, testState",
            crime_index=-1,
            rent_index=-1,
            health_care_index=-1,
            pollution_index=-1,
            climate_index=-1,
            photo="test photo",
        )

        fakeUniversity = University(
            id=-2,
            university_name="test university name",
            city_name="testCity, testState",
            state_name="testState",
            student_size=-1,
            admission_rate=-1,
            academic_year_cost=-1,
            demographics_women=-1,
            demographics_men=-1,
            demographics_white=-1,
            demographics_black=-1,
            demographics_hispanic=-1,
            demographics_asian=-1,
            top_major="test top major",
            photo="test photo",
        )

        try:
            session.add(fakeUniversity)
            session.add(fakeCity)
            session.add(fakeJobCount)
            session.commit()

            jobCountResult = (
                session.query(JobCount).filter_by(profession="test profession").first()
            )
            cityResult = (
                session.query(City).filter_by(city_name="testCity, testState").first()
            )
            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )
            self.assertEqual(str(cityResult.photo), "test photo")

            with app.test_client() as client:
                cities = client.get("/cities/filter?state=testState")
                citiesData = cities.data.decode("UTF-8")
                self.assertTrue('''"city_name": "testCity, testState"''' in citiesData)

            session.delete(jobCountResult)
            session.delete(cityResult)
            session.delete(universityResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_get_cities")

            session.rollback()
            assert False

    def test_get_professions_filtered(self):
        try:
            professionResult = (
                session.query(Profession)
                .filter_by(profession_name="test profession name")
                .first()
            )
            session.delete(professionResult)
            session.commit()
        except:
            pass

        run_everything()

        fakeProfession = Profession(
            id=-2,
            profession_name="test profession name",
            profession_median_wage=-1,
            profession_description="test profession description",
            profession_education="test profession education",
            profession_outlook="test profession outlook",
            profession_video="test profession video",
            profession_projection=1,
            profession_photo="test profession photo",
            profession_major="test profession major",
        )

        try:
            session.add(fakeProfession)
            session.commit()

            professionResult = (
                session.query(Profession)
                .filter_by(profession_name="test profession name")
                .first()
            )
            self.assertEqual(
                str(professionResult.profession_name), "test profession name"
            )

            with app.test_client() as client:
                professions = client.get(
                    "/professions/filter?education=test%20profession%20education&annual_salary=$-10,000%20-%20$30,000"
                )
                professionsData = professions.data.decode("UTF-8")
                self.assertTrue(
                    '"profession_photo": "test profession photo"' in professionsData
                )

            session.delete(professionResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_get_professions")

            session.rollback()
            assert False

    def test_get_universities_filtered(self):
        run_everything()

        try:
            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )
            session.delete(universityResult)
            session.commit()
        except:
            pass

        fakeUniversity = University(
            id=-2,
            university_name="test university name",
            city_name="test city name",
            state_name="test state name",
            student_size=-1,
            admission_rate=-1,
            academic_year_cost=-1,
            demographics_women=-1,
            demographics_men=-1,
            demographics_white=-1,
            demographics_black=-1,
            demographics_hispanic=-1,
            demographics_asian=-1,
            top_major="test top major",
            photo="test photo",
        )

        try:
            session.add(fakeUniversity)
            session.commit()

            universityResult = (
                session.query(University)
                .filter_by(university_name="test university name")
                .first()
            )
            self.assertEqual(
                str(universityResult.university_name), "test university name"
            )

            with app.test_client() as client:
                universities = client.get(
                    "/universities/filter?top_major=test%20top%20major"
                )
                universitiesData = universities.data.decode("UTF-8")
                self.assertTrue('''"demographics_women": -1.0''' in universitiesData)

            session.delete(universityResult)
            session.commit()

        except Exception as e:
            print(e)
            print("error in test_get_professions")

            session.rollback()
            assert False


if __name__ == "__main__":
    DBSession = sessionmaker(bind=engine)
    db.metadata.bind = engine
    session = DBSession()
    main()
