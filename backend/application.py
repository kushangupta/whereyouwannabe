import sys
import os

sys.path.append("/opt/python/current/app/backend")
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
import json
import pprint
import requests
from sqlalchemy import create_engine, or_, func, desc
from sqlalchemy.orm import sessionmaker
from flask_cors import CORS
from flask import request
from flask import Response
from apiModels import (
    engine,
    City,
    Profession,
    University,
    db,
    JobCount,
    app as application,
)

application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
CORS(application)
DBSession = sessionmaker(bind=engine)
db.metadata.bind = engine
global session
session = DBSession()


def convert_to_dict(instances, rounded=True):
    l = []
    for instance in instances:
        result_dict = {}
        instance_dict = instance.__dict__
        instance_dict.pop("_sa_instance_state", None)
        for key, value in instance_dict.items():

            if rounded and "index" in str(key):
                value = float(value)
                result_dict[key] = str(round(value, 2))
            else:
                result_dict[key] = value
        l.append(result_dict)
    return l


def response(d):
    response = Response(json.dumps(d), status="200")
    response.status_code = 200
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("content-type", "application/json")
    response.headers.add("Access-Control-Allow-Headers", "Content-Type,Authorization")
    response.headers.add("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")

    return response


@application.route("/search_cities", methods=["GET", "POST", "OPTIONS"])
def search_cities():
    city_query = request.args.get("search_query", "")
    if city_query:
        try:
            query = session.query(City).all()
            found_query = []
            for each_row in query:
                if city_query.lower() in str(each_row).lower():
                    found_query.append(each_row)
            found_query_dict = convert_to_dict(found_query)
            return response(found_query_dict)
        except:
            print("ERROR when searching for cities")


@application.route("/search_professions", methods=["GET", "POST", "OPTIONS"])
def search_professions():
    profession_query = request.args.get("search_query", "")
    if profession_query:
        try:
            query = session.query(Profession).all()
            found_query = []
            for each_row in query:
                if profession_query.lower() in str(each_row).lower():
                    found_query.append(each_row)
            found_query_dict = convert_to_dict(found_query)
            return response(found_query_dict)
        except:
            print("ERROR when searching for professions")


@application.route("/search_universities", methods=["GET", "POST", "OPTIONS"])
def search_universities():
    universities_query = request.args.get("search_query", "")
    if universities_query:
        try:
            query = session.query(University).all()
            found_query = []
            for each_row in query:
                if universities_query.lower() in str(each_row).lower():
                    found_query.append(each_row)
            found_query_dict = convert_to_dict(found_query)
            return response(found_query_dict)
        except:
            print("ERROR when searching for universities")


@application.route("/cities", methods=["GET", "POST", "OPTIONS"])
def get_cities():
    city_name = request.args.get("city_name", None)
    if request.json:
        page = request.json.get("page", 1)
    else:
        page = 1
    if city_name:
        try:
            city_instance = session.query(City).filter_by(city_name=city_name)
            city_dict = convert_to_dict(city_instance)
            job_count_instance = (
                session.query(JobCount)
                .filter_by(city_name=city_name)
                .order_by(desc(JobCount.job_count))
                .limit(3)
                .all()
            )
            job_count_dict = convert_to_dict(job_count_instance)
            city_dict[0]["job_counts"] = job_count_dict
            city_name = city_name.split(",")[0]
            universities_in_city = (
                session.query(University).filter_by(city_name=city_name).all()
            )
            universities_in_city_dict = convert_to_dict(universities_in_city)
            city_dict[0]["universities"] = universities_in_city_dict
            professions_for_majors = []

            for city in city_dict[0]["job_counts"]:
                tempList = get_professions_for_majors(city["profession"])
                for job in tempList:
                    professions_for_majors.append(job)
            city_dict[0]["profession_majors"] = professions_for_majors

            return response(city_dict)

        except:
            session.rollback()
            return response(None)

    all_cities = City.query.paginate(max_per_page=15, page=page)
    d = convert_to_dict(all_cities.items, True)
    return response(d)


# supplemental API call for API documentation on Postman
@application.route("/top_professions_city", methods=["GET", "POST", "OPTIONS"])
def get_top_professions_in_city():
    city = request.args.get("city_name", None)
    job_count_instance = (
        session.query(JobCount)
        .filter_by(city_name=city)
        .order_by(desc(JobCount.job_count))
        .limit(3)
        .all()
    )
    job_count_dict = convert_to_dict(job_count_instance)
    return response(job_count_dict)


@application.route("/professions", methods=["GET", "POST", "OPTIONS"])
def get_professions():
    profession_name = request.args.get("profession_name", None)
    if request.json:
        page = request.json.get("page", 1)
    else:
        page = 1

    if profession_name:
        try:
            if " or " in profession_name:
                profession_name = profession_name.replace(" or ", "/")
            # retrieve the information for this profession
            profession_instance = (
                session.query(Profession)
                .filter_by(profession_name=profession_name)
                .all()
            )
            profession_dict = convert_to_dict(profession_instance, False)[0]

            # add the best cities information to the profession's information dict
            best_cities = get_best_cities_for_profession(
                profession_dict["profession_major"]
            )
            profession_dict["profession_best_cities"] = best_cities
            # add the best universities information to the profession's information dict
            best_universities = get_best_universities_for_profession(
                profession_dict["profession_major"]
            )
            profession_dict["profession_best_universities"] = best_universities
            return response(profession_dict)
        except:
            session.rollback()
            return response(None)
    else:
        # deal with the case of retrieving all the professions based on pagination
        all_professions = Profession.query.paginate(per_page=15, page=page)
        # array containing dicts representing professions
        profession_dict_array = convert_to_dict(all_professions.items, False)
        count = 0
        for element in profession_dict_array:
            if "/" in element["profession_name"]:
                element["profession_name"] = element["profession_name"].replace(
                    "/", " or "
                )
                count += 1

        # for each profession dict, find and populate its best cities as a new field
        for profession_dict in profession_dict_array:
            profession_name = profession_dict["profession_name"]
            best_cities = get_best_cities_for_profession(
                profession_dict["profession_major"]
            )
            profession_dict["profession_best_cities"] = best_cities

    return response(profession_dict_array)


# given a profession name, finds the (up to) 5 best cities for that profession
def get_best_cities_for_profession(profession_major, extra=5):
    job_count_instance = (
        session.query(JobCount)
        .filter_by(profession=profession_major)
        .order_by(desc(JobCount.job_count))
        .limit(extra)
        .all()
    )
    job_count_dict = convert_to_dict(job_count_instance)
    # keeps track of the (up to) best 5 cities for this job
    best_cities = []
    for job_count_object in job_count_dict:
        best_cities.append(job_count_object["city_name"])
    return best_cities


# supplemental API call for Postman documentation
@application.route("/universities_in_city", methods=["GET", "POST", "OPTIONS"])
def get_universities_in_city():
    city = request.args.get("city_name", None)
    city = city.split(",")
    try:
        universities_in_city = (
            session.query(University).filter_by(city_name=city[0]).all()
        )
    except:
        pass
    universities_in_city_dict = convert_to_dict(universities_in_city)
    return response(universities_in_city_dict)


# supplemental API call for Postman documentation
@application.route("/universities_for_profession", methods=["GET", "POST", "OPTIONS"])
def get_universities_for_profession():
    profession = request.args.get("profession_name", None)
    profession_instance = (
        session.query(Profession).filter_by(profession_name=profession).all()
    )
    profession_dict = convert_to_dict(profession_instance)
    university_instance = (
        session.query(University)
        .filter_by(top_major=profession_dict[0]["profession_major"])
        .all()
    )
    university_dict = convert_to_dict(university_instance)
    return response(university_dict)


# given a profession name, finds the (up to) 5 best universities for that profession (best determined by lowest acceptance rate)
def get_best_universities_for_profession(profession_name):
    university_instance = (
        session.query(University)
        .filter_by(top_major=profession_name)
        .order_by(desc(University.admission_rate))
        .limit(5)
        .all()
    )
    university_dict = convert_to_dict(university_instance)
    # keeps track of the (up to) best 5 universities for this profession
    best_universities = []
    for university_object in university_dict:
        best_universities.append(university_object["university_name"])
    return best_universities


@application.route("/universities", methods=["GET", "POST", "OPTIONS"])
def get_universities():
    university_name = request.args.get("university_name", None)
    if request.json:
        page = request.json.get("page", 1)
    else:
        page = 1
    if university_name:
        try:
            university_instance = (
                session.query(University)
                .filter_by(university_name=university_name)
                .all()
            )
            university_dict = convert_to_dict(university_instance)
            professions_for_majors = get_professions_for_majors(
                university_dict[0]["top_major"]
            )
            university_dict[0]["profession_majors"] = professions_for_majors
            return response(university_dict)
        except:
            session.rollback()
            return response(None)

    all_universities = University.query.paginate(per_page=15, page=page)
    university_dict = convert_to_dict(all_universities.items)
    return response(university_dict)


def get_professions_for_majors(major):
    instance = session.query(Profession).filter_by(profession_major=major).all()
    profession_major_dict = convert_to_dict(instance)
    profession_majors = []
    for profession_object in profession_major_dict:
        if "/" in profession_object["profession_name"]:
            profession_object["profession_name"] = profession_object[
                "profession_name"
            ].replace("/", " or ")
        profession_majors.append(profession_object["profession_name"])
    return profession_majors


@application.route("/universities/filter", methods=["GET", "POST"])
def get_universities_filtered():
    if request.json:
        page = request.json.get("page", 1)
    else:
        page = 1

    state_filter = request.args.get("state", None)
    top_major_filter = request.args.get("top_major", None)
    sort = request.args.get("sort", None)

    queryDict = {"state_name": state_filter, "top_major": top_major_filter}
    filters = []

    for key, val in queryDict.items():
        if val is not None and val != "State" and val != "Major" and val != "All":
            filters.append(University.__table__.c[key] == val)

    if sort is not None and sort != "Sort" and sort != "All":
        universities = (
            University.query.filter(*filters)
            .order_by(University.__table__.c[sort])
            .paginate(max_per_page=15, page=page)
            .items
        )
    else:
        universities = (
            University.query.filter(*filters).paginate(max_per_page=15, page=page).items
        )

    university_dict = convert_to_dict(universities)
    return response(university_dict)


@application.route("/professions/filter", methods=["GET", "POST"])
def get_professions_filtered():
    if request.json:
        page = request.json.get("page", 1)
        print("HERE")
    else:
        page = 1

    education_filter = request.args.get("education", None)
    annual_salary_filter = request.args.get("annual_salary", None)
    sort = request.args.get("sort", None)

    queryDict = {
        "profession_education": education_filter,
        "profession_median_wage": annual_salary_filter,
    }
    filters = []

    for key, val in queryDict.items():
        if (
            val is not None
            and val != "Education"
            and val != "Annual Salary"
            and val != "All"
        ):
            if key == "profession_median_wage":
                ranges = val.split(" - ")
                lowRange = ranges[0].replace("$", "").replace(",", "")
                highRange = ranges[1].replace("$", "").replace(",", "")

                filters.append(
                    Profession.profession_median_wage.between(lowRange, highRange)
                )
            else:
                filters.append(Profession.__table__.c[key] == val)

    if sort is not None and sort != "Sort" and sort != "All":
        result = (
            Profession.query.filter(*filters)
            .order_by(Profession.__table__.c[sort])
            .paginate(max_per_page=15, page=page)
            .items
        )
    else:
        result = (
            Profession.query.filter(*filters).paginate(max_per_page=15, page=page).items
        )

    result_dict = convert_to_dict(result, False)
    return response(result_dict)


@application.route("/cities/filter", methods=["GET", "POST"])
def get_cities_filtered():
    if request.json:
        page = request.json.get("page", 1)
    else:
        page = 1

    state_filter = request.args.get("state", None)
    major_filter = request.args.get("major", None)
    sort = request.args.get("sort", None)

    print("Received major: " + str(major_filter))

    queryDict = {"major": major_filter}
    filters = []

    # special case because this is checking for like/contains and not exact value matching
    if state_filter is not None and state_filter != "State" and state_filter != "All":
        filters.append(City.__table__.c["city_name"].like("%" + ", " + state_filter))

    for key, val in queryDict.items():
        if val is not None and val != "Major" and val != "All":
            if key == "major":
                filters.append(City.__table__.c[key].like("%" + val + "%"))
            else:
                filters.append(City.__table__.c[key] == val)

    if sort is not None and sort != "Sort" and sort != "All":
        result = (
            City.query.filter(*filters)
            .order_by(City.__table__.c[sort])
            .paginate(max_per_page=15, page=page)
            .items
        )
    else:
        result = City.query.filter(*filters).paginate(max_per_page=15, page=page).items

    result_dict = convert_to_dict(result)
    return response(result_dict)

@application.route("/all_professions", methods=["GET", "POST"])
def all_professions():
    instance = session.query(Profession).all()
    profession_major_dict = convert_to_dict(instance)
    fence = 0
    for profession_dict in profession_major_dict:
        profession_dict['label'] = profession_dict.pop('profession_name')
        # print('hi')
        try:
            profession_dict['value'] = len(get_best_cities_for_profession(profession_dict['profession_major'], 10))
            if fence > 35:
                return response(profession_major_dict[:35])
        except:
            pass

        fence += 1

    return response(profession_major_dict)

@application.route("/num_universities", methods=["GET", "POST"])
def num_uni_per_state():
    instance = session.query(University).all()
    states = dict()
    unis = convert_to_dict(instance)
    for uni in unis:
        if uni['state_name'] not in states:
            states[uni['state_name']] = 1
        else:
            states[uni['state_name']] += 1
    return response(states)

@application.route("/num_cities", methods=["GET", "POST"])
def num_cities_per_state():
    instance = session.query(City).all()
    states = dict()
    cities = convert_to_dict(instance)
    for city in cities:
        state_name = city['city_name'][-2:len(city['city_name'])]
        if state_name not in states:
            states[state_name] = 1
        else:
            states[state_name] += 1
    return response(states)

@application.route("/", methods=["GET", "POST", "OPTIONS"])
def test():
    d = {"Hello": "World"}
    return response(d)


def run_everything():
    application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    CORS(application)
    DBSession = sessionmaker(bind=engine)
    db.metadata.bind = engine
    global session
    session = DBSession()


if __name__ == "__main__":
    application.run(debug=True)
