import requests
import pprint
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import json
import sys
from sqlalchemy import create_engine


application = app = Flask(__name__)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql+psycopg2://postgres:blakk12345@wywb.cv2kfb4cxpme.us-east-1.rds.amazonaws.com:5432/"
db = SQLAlchemy(app)


class Profession(db.Model):
    __tablename__ = "Profession"
    __table_args__ = {"schema": "public"}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    profession_name = db.Column(db.String(), nullable=False)
    profession_median_wage = db.Column(db.Float, nullable=True, unique=False)
    profession_description = db.Column(db.String(), nullable=True, unique=False)
    profession_education = db.Column(db.String(), nullable=True, unique=False)
    profession_outlook = db.Column(db.String(), nullable=True, unique=False)
    profession_video = db.Column(db.String(), nullable=True, unique=False)
    profession_projection = db.Column(db.Integer, nullable=True, unique=False)
    profession_photo = db.Column(db.String(), nullable=True)
    profession_major = db.Column(db.String())

    def __repr__(self):
        return f"Profession('{self.id}', '{self.profession_name}', '{self.profession_median_wage}',\
        '{self.profession_description}','{self.profession_education}','{self.profession_outlook}',\
        '{self.profession_video}','{self.profession_photo}', '{self.profession_major}')"


class City(db.Model):
    __tablename__ = "City"
    __table_args__ = {"schema": "public"}
    __table_args__ = {"extend_existing": True}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    city_name = db.Column(db.String(50), unique=True)
    crime_index = db.Column(db.String(50), nullable=False, unique=False)
    rent_index = db.Column(db.String(50), nullable=False, unique=False)
    health_care_index = db.Column(db.String(50), nullable=False, unique=False)
    pollution_index = db.Column(db.String(50), nullable=False, unique=False)
    climate_index = db.Column(db.String(50), nullable=False, unique=False)
    photo = db.Column(db.String())
    major = db.Column(db.String(150))

    def __repr__(self):
        return f"City('{self.id}', '{self.city_name}', '{self.crime_index}','{self.rent_index}',\
        '{self.health_care_index}','{self.pollution_index}','{self.climate_index}','{self.photo}', '{self.major}')"


class JobCount(db.Model):
    __tablename__ = "JobCount"
    __table_args__ = {"schema": "public"}
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    profession = db.Column(db.String(50))
    city_name = db.Column(db.String(50))
    job_count = db.Column(db.Integer)

    def __repr__(self):
        return f"JobCount('{self.id}', '{self.profession}', '{self.city_name}', '{self.job_count}')"


class University(db.Model):
    __tablename__ = "University"
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    university_name = db.Column(db.String(50), nullable=False, unique=True)
    student_size = db.Column(db.Integer, nullable=False, unique=False)
    admission_rate = db.Column(db.Float, nullable=False, unique=False)
    academic_year_cost = db.Column(db.Integer, nullable=False, unique=False)
    demographics_women = db.Column(db.Float, nullable=False, unique=False)
    demographics_men = db.Column(db.Float, nullable=False, unique=False)
    demographics_white = db.Column(db.Float, nullable=False, unique=False)
    demographics_black = db.Column(db.Float, nullable=False, unique=False)
    demographics_hispanic = db.Column(db.Float, nullable=False, unique=False)
    demographics_asian = db.Column(db.Float, nullable=False, unique=False)
    city_name = db.Column(db.String(50), nullable=False)
    state_name = db.Column(db.String(50), nullable=False)
    top_major = db.Column(db.String(50), nullable=False)
    photo = db.Column(db.String())

    def __repr__(self):
        return f"University('{self.id}', '{self.university_name}', '{self.student_size}', \
		'{self.admission_rate}', '{self.academic_year_cost}', '{self.demographics_women}', \
		'{self.demographics_men}', '{self.demographics_white}', '{self.demographics_black}', \
		'{self.demographics_hispanic}', '{self.demographics_asian}', \
		'{self.city_name}', '{self.state_name}', '{self.top_major}, '{self.photo}')"

engine = create_engine(app.config["SQLALCHEMY_DATABASE_URI"])
