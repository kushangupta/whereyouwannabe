import requests
import pprint
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import json
import sys
from apiModels import Profession, City, JobCount, app, db

# app=Flask(__name__)
# app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///wywb.db'
# db = SQLAlchemy(app)

# professions = ['software developer', ' statistician', ' physician assistant', ' dentist', ' nurse anesthetist', ' orthodontist', ' nurse practitioner', ' pediatrician', ' obstetrician and gynecologist', ' oral and maxillofacial surgeon', ' physician', ' prosthodontist', ' occupational Therapist', ' anesthesiologist', ' surgeon', ' nurse midwife', ' mathematician', ' cartographer', ' registered nurse', ' physical therapist', ' clinical laboratory technician', ' podiatrist', ' speech-language pathologist', ' accountant', ' financial manager', ' respiratory therapist', ' computer systems analyst', ' it manager', ' medical and health services manager', ' dental hygienist', ' marketing manager', ' physical therapist assistant', ' actuary', ' operations research analyst', ' diagnostic medical sonographer', ' civil engineer', ' business operations manager', ' landscaper and groundskeeper', ' psychologist', ' information security analyst', ' mechanical engineer', ' lawyer', ' construction manager', ' financial advisor', ' school psychologist', ' political scientist', ' orthotist and prosthetist', ' database administrator', ' optometrist', ' substance abuse and behavioral disorder counselor', ' web developer', ' personal care aide', ' psychiatrist', ' community service manager', ' pharmacist', ' home health aide', ' wind turbine technician', ' architect', ' plumber', ' market research analyst', ' cost estimator', ' hairdresser', ' school counselor', ' veterinarian', ' patrol officer', ' medical assistant', ' hr specialist', ' computer systems administrator', ' industrial psychologist', ' sales manager', ' loan officer', ' medical secretary', ' licensed practical and licensed vocational nurse', ' biochemist', ' marriage and family therapist', ' highschool teacher', ' genetic counselor', ' child and family social worker', ' financial analyst', ' paramedic', ' chiropractor', ' mental health counselor', ' clinical social worker', ' management analyst', ' logistician', ' dietitian and nutritionist', ' environmental engineer', ' massage therapist', ' mri technologist', ' environmental engineering technician', ' phlebotomist', ' pharmacy technician', ' biomedical engineer', ' physical therapist aide', ' technical writer', ' fundraiser', ' maintenance and repair worker', ' interpreter and translator', ' radiation therapist', ' graphic designer']
# professions = ['software developer', ' statistician', ' physician assistant', ' dentist', ' nurse anesthetist', ' orthodontist', ' nurse practitioner', ' pediatrician', ' obstetrician and gynecologist', ' oral and maxillofacial surgeon', ' physician', ' prosthodontist', ' occupational Therapist', ' anesthesiologist', ' surgeon', ' nurse midwife', ' mathematician', ' cartographer', ' registered nurse', ' physical therapist']
professions = [
    "agriculture",
    "resources",
    "architecture",
    "communications",
    "computer",
    "education",
    "engineering",
    "language",
    "legal",
    "english",
    "humanities",
    "library",
    "biologicial",
    "mathematics",
    "military",
    "multidiscipline",
    "psychology",
    "construction",
    "transportation",
    "health",
    "history",
]
# scraped from https://www.niche.com/blog/the-most-popular-college-majors/

# Put our job count data into the database
def updateJobCountData(currentProfession, jobCountData):
    dataLength = 0
    # try and except incase data is faulty and doesn't have a locations field
    try:
        dataLength = len(jobCountData["Locations"])
    except:
        pass

    citiesAdded = 0
    index = 0
    for index in range(dataLength):
        cityName = jobCountData["Locations"][index]["LocationName"]
        # only create this as an entry if this city is found in the City table
        cityExists = City.query.filter_by(city_name=cityName).scalar() is not None
        if cityExists:
            count = jobCountData["Locations"][index]["LocationCount"]
            # print("Adding entry with profession: " + str(currentProfession) + " and job_count: " + str(count) + " and cityName: " + str(cityName))
            db.session.add(
                JobCount(
                    profession=currentProfession, job_count=count, city_name=cityName
                )
            )
            citiesAdded -= -1
        # once we have determined our top 5 results, can break out of the loop
        if citiesAdded >= 5:
            break
        index += 1
    db.session.commit()


# eventually we loop through top 100 professions and just call this method for each profession
# on their website, it allows to put in 'USA' as location, but it fails here for some reason, please fix
def getJobCountData(currentProfession):
    url = (
        "https://api.careeronestop.org/v1/jobsearch/EUpyJ4PbIhIrRzl/"
        + currentProfession
        + "/USA/25/30"
    )
    payload = {
        "Content-Type": "application/json",
        "Authorization": "Bearer /yy/9cwAa4+V26rtI5ZiClRYaXhsCN7QTtH5i7VI2Dq7xLFu2gAy6MA9xZVrGeCKUpholT7TnLo5bDH9hfk77Q==",
    }
    resp = requests.get(url=url, headers=payload)
    return json.loads(resp.text)


def updateCityProfessionCount(currentProfession):
    # data returned is JSON containing info about how many jobs there are for this profession in different cities
    jobCountData = getJobCountData(currentProfession)

    # catch KeyError when API call to get profession data fails and returns empty information
    try:
        updateJobCountData(currentProfession, jobCountData)
    except KeyError as e:
        print("Found a KeyError in updateCityProfessionCount in ApiProfessions.py")
        print(repr(e))


db.create_all()
# go through every profession in our list of professions at the top
for profession in professions:
    updateCityProfessionCount(profession)
