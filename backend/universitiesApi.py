# source env/bin/activate before can run python stuff

import requests
import pprint
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import json
import sys
from apiModels import Profession, City, JobCount, University, app, db

api_key = "vJaaVq1eVrvQMSNhyTUISBKjV85FrJ6hlvniD3LW"

def getUniversity(university_name):
    university_demographics_url = (
        "https://api.data.gov/ed/collegescorecard/v1/schools?_per_page=100&_page=0&api_key="
        + api_key
        + "&school.name="
        + university_name
        + "&fields=school.name,school.city,school.state,2017.admissions.admission_rate.overall,2017.student.size,"
        + "2017.student.demographics.men,2017.student.demographics.women,2017.student.demographics.race_ethnicity.white,"
        + "2017.student.demographics.race_ethnicity.black,2017.student.demographics.race_ethnicity.hispanic,"
        + "2017.student.demographics.race_ethnicity.asian,2017.cost.attendance.academic_year"
    )
    payload = {"Content-Type": "application/json"}
    response = requests.get(url=university_graphics_url, headers=payload)
    jsonRes = json.loads(response.text)

    top_majors_url = (
        "https://api.data.gov/ed/collegescorecard/v1/schools?_per_page=100&_page=0&api_key="
        + api_key
        + "&school.name="
        + university_name
        + "&fields=2017.academics.program_percentage.agriculture,2017.academics.program_percentage.resources,"
        + "2017.academics.program_percentage.architecture,2017.academics.program_percentage.communication,"
        + "2017.academics.program_percentage.computer,2017.academics.program_percentage.education,2017.academics.program_percentage.engineering,"
        + "2017.academics.program_percentage.language,2017.academics.program_percentage.legal,2017.academics.program_percentage.english,"
        + "2017.academics.program_percentage.humanities,2017.academics.program_percentage.library,2017.academics.program_percentage.biological,"
        + "2017.academics.program_percentage.mathematics,2017.academics.program_percentage.military,2017.academics.program_percentage.multidiscipline,"
        + "2017.academics.program_percentage.psychology,2017.academics.program_percentage.construction,2017.academics.program_percentage.transportation,"
        + "2017.academics.program_percentage.health,2017.academics.program_percentage.history"
    )
    payload = {"Content-Type": "application/json"}
    responseMajors = requests.get(url=top_majors_url, headers=payload)
    jsonResMajors = json.loads(responseMajors.text)

    try:
        school_name = jsonRes["results"][0]["school.name"]
        school_size = jsonRes["results"][0]["2017.student.size"]
        admission_rate = jsonRes["results"][0]["2017.admissions.admission_rate.overall"]
        academic_year_cost = jsonRes["results"][0]["2017.cost.attendance.academic_year"]
        demographics_women = jsonRes["results"][0]["2017.student.demographics.women"]
        demographics_men = jsonRes["results"][0]["2017.student.demographics.men"]
        demographics_white = jsonRes["results"][0]["2017.student.demographics.race_ethnicity.white"]
        demographics_black = jsonRes["results"][0]["2017.student.demographics.race_ethnicity.black"]
        demographics_hispanic = jsonRes["results"][0]["2017.student.demographics.race_ethnicity.hispanic"]
        demographics_asian = jsonRes["results"][0]["2017.student.demographics.race_ethnicity.asian"]
        city_name = jsonRes["results"][0]["school.city"]
        state_name = jsonRes["results"][0]["school.state"]
        max = 0
        top_major = ""
        for item in jsonResMajors["results"][0]:
            if float(jsonResMajors["results"][0][item]) > max:
                max = jsonResMajors["results"][0][item]
                top_major = item.split(".")[3]
        exists = (
            University.query.filter_by(university_name=school_name).scalar() is not None
        )
        if not exists and None not in (
            school_name,
            school_size,
            admission_rate,
            academic_year_cost,
            demographics_women,
            demographics_men,
            demographics_white,
            demographics_black,
            demographics_hispanic,
            demographics_asian,
            state_name,
            city_name,
            top_major,
        ):
            db.session.add(
                University(
                    university_name=school_name,
                    student_size=school_size,
                    admission_rate=admission_rate,
                    academic_year_cost=academic_year_cost,
                    demographics_women=demographics_women,
                    demographics_men=demographics_men,
                    demographics_white=demographics_white,
                    demographics_black=demographics_black,
                    demographics_hispanic=demographics_hispanic,
                    demographics_asian=demographics_asian,
                    state_name=state_name,
                    city_name=city_name,
                    top_major=top_major,
                )
            )
            db.session.commit()

    except KeyError:
        pass

for uni in universities:
    getUniversity(uni)
    print(uni)
