import requests
import pprint
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import json
import sys
from apiModels import Profession, City, JobCount, app, db


majors = [
    "agriculture",
    "resources",
    "architecture",
    "communications",
    "computer",
    "education",
    "engineering",
    "language",
    "legal",
    "english",
    "humanities",
    "library",
    "biologicial",
    "mathematics",
    "military",
    "multidiscipline",
    "psychology",
    "construction",
    "transportation",
    "health",
    "history",
]
# majors = ['psychology']

seen_professions = set()
occupation_codes = []

# first api
# just added functions to access the first api, not really sure everything fits together yet
def getOccupationCodes(data):
    occupationlist = []
    length = 0
    try:
        length = min(len(data["OccupationList"]), 4)
    except KeyError as e:
        pass

    for i in range(length):
        str = data["OccupationList"][i]["OnetCode"]
        occupationlist.append(str)
    return occupationlist


def getInfo(profession):
    url = (
        "https://api.careeronestop.org/v1/occupation/EUpyJ4PbIhIrRzl/"
        + profession
        + "/N/0/10"
    )
    payload = {
        "Content-Type": "application/json",
        "Authorization": "Bearer /yy/9cwAa4+V26rtI5ZiClRYaXhsCN7QTtH5i7VI2Dq7xLFu2gAy6MA9xZVrGeCKUpholT7TnLo5bDH9hfk77Q==",
    }
    resp = requests.get(url=url, headers=payload)
    return json.loads(resp.text)


def callOccupationsList(currentProfession):
    data = getInfo(currentProfession)
    try:
        return getOccupationList(data)
        # print(occupations)
    except:
        pass


# 1st api to gather occupation codes
# go through each major and get 5 occupations related to that major
occupation_codes = {}
for major in majors:
    majorData = getInfo(major)
    occupationCodesThisMajor = getOccupationCodes(majorData)
    # add each occupation code to the global occupation_codes list
    for occupation_code in occupationCodesThisMajor:
        occupation_codes[occupation_code] = major
        # print(occupation_code + 'and' + occupation_codes[occupation_code])


# call second api with occupation code
def generalInfoAPI(occupation_code):
    url = (
        "https://api.careeronestop.org/v1/occupation/EUpyJ4PbIhIrRzl/"
        + occupation_code
        + "/US?training=true&interest=false&videos=false&tasks=false&dwas=false&wages=true&alternateOnetTitles=false&projectedEmployment=true&ooh=false&stateLMILinks=false&relatedOnetTitles=false&skills=false&knowledge=false&ability=false&trainingPrograms=false"
    )
    payload = {
        "Content-Type": "application/json",
        "Authorization": "Bearer /yy/9cwAa4+V26rtI5ZiClRYaXhsCN7QTtH5i7VI2Dq7xLFu2gAy6MA9xZVrGeCKUpholT7TnLo5bDH9hfk77Q==",
    }
    ans = requests.get(url=url, headers=payload)
    return json.loads(ans.text)


# gathered data from api about a profession, they all for sure work
def updateProfessionDatabase(data, major):
    try:
        profession_name = data["OccupationDetail"][0]["OnetTitle"]
        ag = (
            "Agricultural Workers, All Other",
            "Precision Agriculture Technicians",
            "Agricultural Equipment Operators",
            "Career/Technical Education Teachers, Secondary School",
            "Human Resources Managers",
            "Human Resources Specialists",
            "Human Resources Assistants, Except Payroll and Timekeeping",
            "Architectural Drafters",
        )
        if profession_name not in seen_professions and profession_name not in ag:

            profession_median_wage = data["OccupationDetail"][0]["Wages"][
                "NationalWagesList"
            ][0]["Median"]
            profession_description = data["OccupationDetail"][0]["OnetDescription"]
            profession_education = data["OccupationDetail"][0]["EducationTraining"][
                "EducationTitle"
            ]
            profession_outlook = data["OccupationDetail"][0]["BrightOutlookCategory"]
            profession_video = data["OccupationDetail"][0]["COSVideoURL"]
            profession_projection = data["OccupationDetail"][0]["Projections"][
                "Projections"
            ][0][
                "ProjectedAnnualJobOpening"
            ]  # 20 year projections
            profession_major = major

            # print("Adding an entry with name: " + profession_name + " and median_wage" + profession_median_wage)
            # db.session.add(Profession(profession_name=profession_name))
            if profession_name != "military":
                db.session.add(
                    Profession(
                        profession_name=profession_name,
                        profession_median_wage=profession_median_wage,
                        profession_description=profession_description,
                        profession_education=profession_education,
                        profession_outlook=profession_outlook,
                        profession_video=profession_video,
                        profession_projection=profession_projection,
                        profession_major=profession_major,
                    )
                )
            # print("After add")
            seen_professions.add(profession_name)
    except:
        pass


def updateGeneralInfo(occupation_code, major):
    data = generalInfoAPI(occupation_code)
    try:
        updateProfessionDatabase(data, major)
        # print(occupations)
    except:
        pass


# db.session.query(Profession).delete()
# it = Profession.query.filter_by(profession_name='military').first()
# db.session.delete(it)
# db.session.commit()
# db.create_all()
counter = 0
# 2nd api to gather info about professions
for key, value in occupation_codes.items():
    updateGeneralInfo(key, value)
    # print(key, value)
    counter -= -1
    if counter % 30 == 0:
        print("Counter is at: " + str(counter))

db.session.commit()
db.session.close()
print("DONE")
