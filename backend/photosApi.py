import requests
import pprint
from flask_sqlalchemy import SQLAlchemy
from flask import Flask
import json
import sys
from apiModels import Profession, City, JobCount, University, app, db


def parse(instance):
    return instance.split()


def parse_city(instance):
    return instance.split(", ")


def convert_to_url(instance):
    return instance.replace(" ", "+")


def convert_city_to_url(instance):
    return instance.replace(", ", "+")


def convert_commas_to_space(instance):
    return instance.replace(", ", " ")


def getPhoto(query):
    url = (
        "https://api.cognitive.microsoft.com/bing/v7.0/images/search?q="
        + query
        + "&count=10&offset=0&mkt=en-us&safeSearch=Moderate"
    )
    payload = {"Ocp-Apim-Subscription-Key": "573ea6c965af47379e22249e658888b7"}
    response = requests.get(url=url, headers=payload)
    # response = requests.get(url=url)
    # print(response.text)
    return json.loads(response.text)


def updateProfessionPhoto(query, instance_name):
    try:
        # print(words)
        prof = Profession.query.filter_by(profession_name=instance_name).first()
        photoData = getPhoto(query)
        # print(photo_url)
        if photoData["totalEstimatedMatches"] == 0:
            photo_url = "http://firstavenuewealth.com.au/wp-content/uploads/2017/06/O6D1AA0-1-1024x683.jpg"
        else:
            photo_url = photoData["value"][0]["contentUrl"]
        prof.profession_photo = photo_url
        db.session.commit()
    except:
        pass


# professions = Profession.query.all()
# for i in professions:
#  	#print(i.profession_name)
# 	words = convert_commas_to_space(i.profession_name)
# 	words = convert_to_url(words)
# 	updateProfessionPhoto(words, i.profession_name)
# 	print(Profession.query.filter_by(profession_name=i.profession_name).first())

# prof = Profession.query.filter_by(profession_name='Area, Ethnic, and Cultural Studies Teachers, Postsecondary').first()
# query = convert_commas_to_space('Area, Ethnic, and Cultural Studies Teachers, Postsecondary')
# query = convert_to_url(query)
# print(query)
# updateProfessionPhoto(query, 'Area, Ethnic, and Cultural Studies Teachers, Postsecondary')
# print(Profession.query.filter_by(profession_name='Area, Ethnic, and Cultural Studies Teachers, Postsecondary').first())

# bing key:983db932bf454afebd0102ea94413f63
def getPhotoForUniversity(instance):
    searchquery = convert_to_url(instance)
    print(searchquery)
    url = (
        "https://api.cognitive.microsoft.com/bing/v7.0/images/search?q="
        + searchquery
        + "&count=10&offset=0&mkt=en-us&safeSearch=Moderate"
    )
    payload = {"Ocp-Apim-Subscription-Key": "573ea6c965af47379e22249e658888b7"}
    response = requests.get(url=url, headers=payload)
    # print(response.text)
    return json.loads(response.text)


def updateUniversityPhoto(instance):
    try:
        uni = University.query.filter_by(university_name=instance).first()
        photoData = getPhotoForUniversity(instance)
        if photoData["totalEstimatedMatches"] == 0:
            photo_url = "https://www.ashland.edu/sites/default/files/styles/panopoly_image_original/public/foundersalw_6106_mobile.jpg?itok=TE8n_Om6"
        else:
            photo_url = photoData["value"][0]["contentUrl"]
        uni.photo = photo_url

        db.session.commit()
    except:
        pass


# universities = University.query.all()
# for i in universities:
# 	updateUniversityPhoto(i.university_name)
# 	print(University.query.filter_by(university_name=i.university_name).first())


def getPhotoForCity(instance):
    # print('here')
    searchquery = convert_city_to_url(instance)
    # print(searchquery)
    searchquery += "+city"
    # print(searchquery)
    url = (
        "https://api.cognitive.microsoft.com/bing/v7.0/images/search?q="
        + searchquery
        + "&count=10&offset=0&mkt=en-us&safeSearch=Moderate"
    )
    payload = {"Ocp-Apim-Subscription-Key": "573ea6c965af47379e22249e658888b7"}
    response = requests.get(url=url, headers=payload)
    # print(url)
    # print(response.text)
    return json.loads(response.text)


def updateCityPhoto(instance):
    try:
        my_city = City.query.filter_by(city_name=instance).first()
        # print('here')
        photoData = getPhotoForCity(instance)
        if photoData["totalEstimatedMatches"] != 0:
            photo_url = photoData["value"][0]["contentUrl"]
        else:
            photo_url = "http://cityofmillersburg.org/wp-content/uploads/2016/05/City-of-millersburg-Oregon-city-park-66-1.jpg"
        my_city.photo = photo_url
        db.session.commit()
    except:
        pass


# cities = City.query.all()
# for i in cities:
#  	updateCityPhoto(i.city_name)
#  	print(City.query.filter_by(city_name=i.city_name).first())
# updateCityPhoto('Denton, TX')
# print(City.query.filter_by(city_name=('Denton, TX')).first())
