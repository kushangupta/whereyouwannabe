import React, {Component} from 'react';
import Navigation from '../Components/Navigation.js'
import Card from "react-bootstrap/Card";
import axios from "axios";

class ProfessionInstance extends Component {
    state = {
        data: null,
    };

    componentDidMount() {
        let urlSplit = String(window.location.href).split('/');
        let professionName = urlSplit[urlSplit.length - 1];
        axios.get(`https://api.whereyouwannabe.org/professions?profession_name=` + professionName)
            .then(res => {
                const ourData = res.data;
                this.setState({data: ourData});
            })
    }

    renderUniversityLinks() {
        if (this.state.data.profession_best_universities.length == 0) {
            return (
                <div>
                    None
                </div>
            )
        }
        return this.state.data.profession_best_universities.map((uni) => {
            return (
                <div>
                    <Card.Link href={("/universities/" + String(uni))}>{uni}</Card.Link>
                </div>
            )
        })
    }

    renderCitiesLinks() {
        return this.state.data.profession_best_cities.map((city) => {
            return (
                <div>
                    <Card.Link href={("/cities/" + String(city))}>{city}</Card.Link>
                </div>
            )
        })
    }

    renderVideoLinks() {
        return (
            <div>
                <Card.Link href={(this.state.data.profession_video)} target="_blank">{"Video"}</Card.Link>
            </div>
        )
    }

    render() {
        if (this.state.data == null) {
            return <b>Loading...</b>
        } else {
            return (
                <div>
                    <Navigation/>
                    <body class="Instance center-div">
                    <h2 class="Instance-header">{this.state.data.profession_name}</h2>
                    <div class="info" align="center">
                        <img class="instance-photo row" src={this.state.data.profession_photo} alt=""/>
                        <Card class="instance-text row" style={{width: '34rem'}}>
                            <Card.Body>
                                <Card.Title>{this.state.data.profession_name}</Card.Title>
                                <Card.Text>
                                    Description: {this.state.data.profession_description}
                                </Card.Text>
                                <Card.Text>
                                    Median Wage: ${this.state.data.profession_median_wage}
                                </Card.Text>
                                <Card.Text>
                                    Suggested Education: {this.state.data.profession_education}
                                </Card.Text>
                                <Card.Text>
                                    Future Outlook: {this.state.data.profession_outlook}
                                </Card.Text>
                                <Card.Text>
                                    Video: {this.renderVideoLinks()}
                                </Card.Text>
                                <Card.Text>
                                    Projection: {this.state.data.profession_projection}
                                </Card.Text>
                                <Card.Text>
                                    Top cities for this profession:
                                    {this.renderCitiesLinks()}
                                </Card.Text>
                                <Card.Text>
                                    Top Universities educating this profession:
                                    {this.renderUniversityLinks()}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                    </body>
                </div>
            );
        }
    }
}

export default ProfessionInstance;
