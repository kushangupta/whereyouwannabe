import React, {Component} from 'react';
import {ButtonToolbar, Dropdown, Nav} from 'react-bootstrap';
import Navigation from '../Components/Navigation.js'
import CardDeck from "react-bootstrap/CardDeck";
import Card from "react-bootstrap/Card";
import Button from 'react-bootstrap/Button'
import axios from 'axios';
import '../Styles/Model.css';
import ProfessionsFilterMenu from '../Components/ProfessionsFilterMenu.js';
import Footer from '../Components/Footer';

import '../Styles/Instance.css';

class Professions extends Component {
    state = {
        professions: null,
        page: 1,
        educationFilterText: "Education",
        salaryFilterText: "Annual Salary",
        apiLink: "https://api.whereyouwannabe.org/professions",
        SortText: "Sort",
        displayedSortText: "Sort",
        search: "search_professions"
    };

    sortMap = {
        "Name": "profession_name",
        "Median Wage": "profession_median_wage",
        "Projection": "profession_projection"
    };

    componentDidMount() {
        axios.get(this.state.apiLink)
            .then(res => {
                const professions = res.data;
                this.setState({professions});
            })
    }

    createProfessionRows = (professions) => {
        let rows = [];
        for (let i = 0; i < 5; i++) {
            let children = [];
            for (let j = 0; j < 3; j++) {
                if (professions.length <= j + 3 * i) {
                    break
                }
                var profession = professions[j + (3 * i)];
                children.push(
                    <Card>
                        <Nav.Link className="profession-instance"
                                  href={("/professions/" + String(profession.profession_name))}>
                            <Card.Img variant="top" class="photo" src={profession.profession_photo}/>
                            <Card.Body>
                                <Card.Title>{profession.profession_name}</Card.Title>
                                <Card.Text>
                                    Suggested Education: {profession.profession_education}
                                </Card.Text>
                                <Card.Text>
                                    Median Wage: {profession.profession_median_wage}
                                </Card.Text>
                                <Card.Text>
                                    Projection: {profession.profession_projection} new jobs next year
                                </Card.Text>
                            </Card.Body>
                        </Nav.Link>
                    </Card>
                )
            }
            rows.push(<br></br>);
            rows.push(<div className="row">{children}</div>);
            if (professions.length < 3 * i) break
        }
        return rows
    };

    handleSubmit = (event, pageNum) => {
        event.preventDefault();

        const page = pageNum;

        if (page != 0) {
            axios.post(this.state.apiLink, {page})
                .then(res => {
                    const professions = res.data;

                    window.scrollTo(0, 0);
                    this.setState({professions});
                    this.setState({page});
                })
        }
    };

    setter = (apiLink) => {
        this.setState({apiLink})
  
        axios.get(this.state.apiLink)
        .then(res => {
            const professions = res.data;
            this.setState({ professions });
        })
    }

    handleButtons = (page) => {
      let buttons = []
      if(page != 1){
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, 1)}}>
            first
          </Button>
        )
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, page - 1)}}>
            &lt;&lt;
          </Button>
        )
      }
      buttons.push(
        <Button variant='primary' id='curr-page' disabled>
        {page}
        </Button>
      )
      if(page != 5){
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, page + 1)}}>
            &gt;&gt;
          </Button>
        )
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, 5)}}>
            last
          </Button>
        )
      }
      return buttons
    }


    render() {
        const search = this.state.search;
        if (this.state.professions === null) {
            return <b> Loading... </b>
        } else {
            return (
                <div>
                    <Navigation/>
                    <div class="Model">
                        <header class="Model-header">
                            <h2 className='Cities-header'>Professions</h2>
                            <h5>What type of profession are you? <br/>Amount of money you could make in this city<br/>
                                Find what's best for you</h5>

                            <div className='row d-flex justify-content-center'>
                                <ProfessionsFilterMenu setter={this.setter}/>
                            </div>

                            <CardDeck className="ModelDeck">
                                {this.createProfessionRows(this.state.professions)}
                            </CardDeck>
                            {this.handleButtons(this.state.page)}
                        </header>
                    </div>
                    <Footer/>
                </div>
            );
        }
    }
}

export default Professions;