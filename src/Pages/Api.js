import React, {Component} from 'react';
import Navigation from '../Components/Navigation.js'
import Card from "react-bootstrap/Card";
import '../Styles/Home.css';
import cityphoto from '../static_resources/homephoto.jpg';
import '../Styles/Api.css';

class Api extends Component {
    render() {
        return (
            <div>
                <Navigation/>
                <body id='mybody' background={cityphoto}>
                <header className="Home-header">
                    <Card.Link className="apilink"
                               href={'https://documenter.getpostman.com/view/7263584/SVtR1AA3?version=latest'}>
                        Check out our API!</Card.Link>
                </header>
                </body>
            </div>

        );
    }
}

export default Api;
