import React, {Component} from "react";
import Navigation from "../Components/Navigation";
import '../Styles/Model.css';
import BubbleChart from "../Components/BubbleChart";
import CitiesBarGraph from '../Components/CitiesBarGraph';
import StateMap from '../Components/StateMap.js';
import * as d3 from "d3";
import Footer from '../Components/Footer.js';
import CustomerBarChart from "../Components/CustomerBarChart";
import CustomerBubbleChart from "../Components/CustomerBubbleChart";
import CustomerPieChart from "../Components/CustomerPieChart";

class Visualizations extends Component {
    state = {
        showBubbleChart: false,
        showBarGraph: true,
        showMap: false,
        showCustomerVisualizations: false
    };

    testMapData() {
        var sampleData ={};	/* Sample random data. */
        ["HI", "AK", "FL", "SC", "GA", "AL", "NC", "TN", "RI", "CT", "MA",
        "ME", "NH", "VT", "NY", "NJ", "PA", "DE", "MD", "WV", "KY", "OH",
        "MI", "WY", "MT", "ID", "WA", "DC", "TX", "CA", "AZ", "NV", "UT",
        "CO", "NM", "OR", "ND", "SD", "NE", "IA", "MS", "IN", "IL", "MN",
        "WI", "MO", "AR", "OK", "KS", "LS", "VA"]
		.forEach(function(d){
			var low=Math.round(100*Math.random()),
				mid=Math.round(100*Math.random()),
				high=Math.round(100*Math.random());
			sampleData[d]={low:d3.min([low,mid,high]), high:d3.max([low,mid,high]),
					avg:Math.round((low+mid+high)/3), color:d3.interpolate("#ffffcc", "#800026")(low/100)};
		});
        return sampleData
    }

    render() {
        return (
            <div>
                <body className='background'>

                <Navigation/>
                <header>
                    <div className="btn-group d-flex" role="group" aria-label="...">
                        <button type="button" className="btn btn-primary" id='dropdown-btn' onClick={() => {
                            this.setState({
                                showBubbleChart: false,
                                showBarGraph: true,
                                showMap: false,
                                showCustomerVisualizations: false
                            });
                        }}>
                            Bar Chart
                        </button>
                        <button type="button" className="btn btn-primary" id='dropdown-btn' onClick={() => {
                            this.setState({
                                showBubbleChart: true,
                                showBarGraph: false,
                                showMap: false,
                                showCustomerVisualizations: false
                            });
                        }}>Bubble Chart
                        </button>
                        <button type="button" className="btn btn-primary" id='dropdown-btn' onClick={() => {
                            this.setState({
                                showBubbleChart: false,
                                showBarGraph: false,
                                showMap: true,
                                showCustomerVisualizations: false
                            });
                        }}>Map
                        </button>
                        <button type="button" className="btn btn-primary" id='dropdown-btn' onClick={() => {
                            this.setState({
                                showBubbleChart: false,
                                showBarGraph: false,
                                showMap: false,
                                showCustomerVisualizations: true
                            });
                        }}>Customer Visualizations
                        </button>
                    </div>
                    <h2 className='Viz-header'>Visualizations</h2>
                    {this.state.showBarGraph && <h4 className='Viz-header'>Displays the number of cities in a state as reflected from our data</h4>}
                    {this.state.showBubbleChart && <h4 className='Viz-header'>Displays the number of cities the job is in</h4>}
                    {this.state.showMap && <h4 className='Viz-header'>Displays the number of universities in a state as reflected from our data</h4>}
                    {this.state.showCustomerVisualizations && <h4 className='Viz-header'>Displays visualizations of our customer's data</h4>}
                </header>
                {this.state.showBubbleChart &&
                <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <BubbleChart/>
                </div>
                }
                {this.state.showBarGraph &&
                <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <CitiesBarGraph/>
                </div>

                }
                {this.state.showMap &&
                <div style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <StateMap/>
                </div>
                }

                {this.state.showCustomerVisualizations &&
                <body>
                    <div style={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        <CustomerBarChart/>
                    </div>
                    <div style={{display:'flex', alignItems:'center', justifyContent:'center',}}>
                        <CustomerBubbleChart/>
                    </div>
                    <div style={{display:'flex', alignItems:'center', justifyContent:'center',}}>
                        <CustomerPieChart/>
                    </div>
                </body>
                }
                <Footer/>
                </body>
            </div>
        );
    }
}

export default Visualizations;
