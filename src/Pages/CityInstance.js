import React, {Component} from 'react';
import Navigation from '../Components/Navigation.js'
import Card from "react-bootstrap/Card";
import axios from "axios";

class CityInstance extends Component {
    state = {
        data: null,
    };

    componentDidMount() {
        let urlSplit = String(window.location.href).split('/');
        let cityName = urlSplit[urlSplit.length - 1];
        axios.get(`https://api.whereyouwannabe.org/cities?city_name=` + cityName)
            .then(res => {
                const data = res.data;
                this.setState({data});
            })
    }

    renderUniversityLinks() {
        if (this.state.data[0].universities.length == 0) {
            return (
                <div>
                    None
                </div>
            )
        }
        return this.state.data[0].universities.map((uni) => {
            return (
                <div>
                    <Card.Link href={("/universities/" + String(uni.university_name))}>{uni.university_name}</Card.Link>
                </div>
            )
        })
    }

    renderProfessionsLinks() {
        return this.state.data[0].job_counts.map((job) => {
            if (job.profession.indexOf('/') !== -1) {
                return (
                    null
                )
            }
            return (
                <div>
                    <Card.Text href={("/professions/" + String(job.profession))}>{job.profession}</Card.Text>
                </div>
            )
        })
    }

    renderMajorProfessionsLinks() {
        if (this.state.data[0].profession_majors.length == 0) {
            return (
                <div>
                    None
                </div>
            )
        }
        return this.state.data[0].profession_majors.map((job) => {
            return (
                <div>
                    <Card.Link href={("/professions/" + String(job))}>{job}</Card.Link>
                </div>
            )
        })
    }

    render() {
        if (this.state.data == null) {
            return <b>Loading...</b>
        } else {
            return (
                <div>
                    <Navigation/>
                    <body class="Instance center-div">
                    <h2 class="Instance-header">{this.state.data[0].city_name}</h2>
                    <div class="info" align="center">
                        <img class="instance-photo row" src={this.state.data[0].photo} alt=""/>
                        <Card class="instance-text row" style={{width: '34rem'}}>
                            <Card.Body>
                                <Card.Title>{this.state.data[0].city_name}</Card.Title>
                                <Card.Text>
                                    Universities:
                                    {this.renderUniversityLinks()}
                                </Card.Text>
                                <Card.Text>
                                    Top major:
                                    {this.renderProfessionsLinks()}
                                </Card.Text>
                                <Card.Text>
                                    Applicable Professions:
                                    {this.renderMajorProfessionsLinks()}
                                </Card.Text>
                                <Card.Text>
                                    Rent Index: {this.state.data[0].rent_index}
                                </Card.Text>
                                <Card.Text>
                                    Pollution Index: {this.state.data[0].pollution_index}
                                </Card.Text>
                                <Card.Text>
                                    Climate Index: {this.state.data[0].climate_index}
                                </Card.Text>
                                <Card.Text>
                                    Crime Index: {this.state.data[0].crime_index}
                                </Card.Text>
                                <Card.Text>
                                    Health Care Index: {this.state.data[0].health_care_index}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                    </body>
                </div>
            );
        }
    }
}

export default CityInstance;
