import React, {Component} from 'react';
import Navigation from '../Components/Navigation.js'
import Card from "react-bootstrap/Card";
import axios from "axios";

class UniversityInstance extends Component {
    state = {
        data: null,
    };

    componentDidMount() {
        let urlSplit = String(window.location.href).split('/');
        let universityName = urlSplit[urlSplit.length - 1];
        axios.get(`https://api.whereyouwannabe.org/universities?university_name=` + universityName)
            .then(res => {
                const data = res.data;
                this.setState({data});
            })
    }

    renderCityLink() {
        return (
            <div>
                <Card.Link
                    href={("/cities/" + String(this.state.data[0].city_name) + ",%20" + String(this.state.data[0].state_name))}>
                    {this.state.data[0].city_name}, {this.state.data[0].state_name}
                </Card.Link>
            </div>
        )
    }

    renderMajorProfessionsLinks() {
        if (this.state.data[0].profession_majors.length == 0) {
            return (
                <div>
                    None
                </div>
            )
        }
        return this.state.data[0].profession_majors.map((job) => {
            if (job.indexOf('/') !== -1) {
                return (
                    null
                )
            }
            return (
                <div>
                    <Card.Link href={("/professions/" + String(job))}>{job}</Card.Link>
                </div>
            )
        })
    }

    render() {
        if (this.state.data == null) {
            return <b>Loading...</b>
        } else {
            var admissionRate = Math.round(this.state.data[0].admission_rate * 100);
            var women = Math.round(this.state.data[0].demographics_women * 100);
            var men = Math.round(this.state.data[0].demographics_men * 100);
            var white = Math.round(this.state.data[0].demographics_white * 100);
            var black = Math.round(this.state.data[0].demographics_black * 100);
            var hispanic = Math.round(this.state.data[0].demographics_hispanic * 100);
            var asian = Math.round(this.state.data[0].demographics_asian * 100);

            return (
                <div>
                    <Navigation/>
                    <body class="Instance center-div">
                    <h2 class="Instance-header">{this.state.data[0].university_name}</h2>
                    <div class="info" align="center">
                        <img class="instance-photo row" src={this.state.data[0].photo} alt=""/>
                        <Card class="instance-text row" style={{width: '34rem'}}>
                            <Card.Body>
                                <Card.Title>{this.state.data[0].university_name}</Card.Title>
                                <Card.Text>
                                    City:
                                    {this.renderCityLink()}
                                </Card.Text>
                                <Card.Text>
                                    Top major: {this.state.data[0].top_major}
                                </Card.Text>
                                <Card.Text>
                                    Associated Professions:
                                    {this.renderMajorProfessionsLinks()}
                                </Card.Text>
                                <Card.Text>
                                    Academic Year Cost: ${this.state.data[0].academic_year_cost}
                                </Card.Text>
                                <Card.Text>
                                    Admission Rate: {admissionRate}%
                                </Card.Text>
                                <Card.Text>
                                    Student Body Population: {this.state.data[0].student_size}
                                </Card.Text>
                                <br/>
                                <Card.Title>
                                    Demographics
                                </Card.Title>
                                <Card.Text>
                                    women: {women}%
                                </Card.Text>
                                <Card.Text>
                                    men: {men}%
                                </Card.Text>
                                <Card.Text>
                                    white: {white}%
                                </Card.Text>
                                <Card.Text>
                                    black: {black}%
                                </Card.Text>
                                <Card.Text>
                                    hispanic: {hispanic}%
                                </Card.Text>
                                <Card.Text>
                                    asian: {asian}%
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                    </body>
                </div>
            );
        }
    }
}

export default UniversityInstance;
