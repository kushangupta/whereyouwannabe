import React, {Component} from "react";
import Navigation from "../Components/Navigation";
import Card from "react-bootstrap/Card";
import '../Styles/Model.css';
import axios from "axios";
import {Nav} from "react-bootstrap";
import CardDeck from "react-bootstrap/CardDeck";


class Search extends Component {

    state = {
        search_query: null,
        search: null,
        page: 1,
        query: null,
        type_of_search: this.props.search
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let urlSplit = String(window.location.href).split('/');
        let type_of_search = urlSplit[urlSplit.length - 2];
        let query = urlSplit[urlSplit.length - 1].replace(/%20/g, " ");
        this.setState({query});
        this.setState({type_of_search});
        axios.get(`https://api.whereyouwannabe.org/` + type_of_search + `?search_query=` + query)
            .then(res => {
                const search = res.data;
                this.setState({search});
            })
    }

    createSearchResultRows = (search) => {
        let elements = [];
        let instanceTitle = "";
        for(let i = 0; i < search.length; i++){
            var currentInstance = search[i];
            let parsedData = this.displayText(currentInstance);
            if(parsedData.length != 0) {
                let str = [];
                for (var key in currentInstance) {
                    if(key === "profession_photo"){
                        currentInstance['photo'] = currentInstance[key]
                    }
                    if(key.includes("photo") || key.includes("video"))
                        continue;
                    if (key === "university_name" || key === "city_name" || key === "profession_name"){
                        if(currentInstance['university_name']){
                            instanceTitle = String(currentInstance['university_name'])
                        } else{
                            instanceTitle = String(currentInstance[key])
                        }
                    }
                    var keyWithoutUnderscores = key.replace(/_/g, " ");
                    str.push(
                        <text> {keyWithoutUnderscores}: </text>
                    );
                    if(String(currentInstance[key]).toLowerCase().includes(this.state.query.toLowerCase())){
                        var stringLen = String(currentInstance[key]).length;
                        if(stringLen == 0)
                            continue;
                        var startIndex = 0, index, indices = [];
                        var currentString = String(currentInstance[key]).toLowerCase();
                        while ((index = currentString.indexOf(this.state.query.toLowerCase(), startIndex)) > -1) {
                            indices.push(index);
                            startIndex = index + this.state.query.length;
                        }
                        var indicesIndex = 0;
                        var parsedIndex = 0;
                        startIndex = 0;
                        while(parsedIndex < String(currentInstance[key]).length) {
                            var subString = "";
                            if(String(currentInstance[key]).substr(parsedIndex, this.state.query.length).toLowerCase() === (this.state.query.toLowerCase())) {
                                subString = String(currentInstance[key]).substr(parsedIndex, this.state.query.length);
                                parsedIndex += this.state.query.length;
                                indicesIndex++;
                                str.push(
                                    <text className="highlight">{subString}</text>
                                )
                            } else {
                                if (indicesIndex < indices.length) {
                                    subString = String(currentInstance[key]).substr(parsedIndex, indices[indicesIndex] - parsedIndex);
                                    parsedIndex += (indices[indicesIndex] - parsedIndex)
                                } else {
                                    subString = String(currentInstance[key]).substr(parsedIndex);
                                    parsedIndex += subString.length
                                }
                                str.push(
                                    <text>{subString}</text>
                                )
                            }     
                        } 
                    }
                    else {
                        str.push(
                            <text>{search[i][key]} </text>
                        )
                    }
                }
                elements.push(
                    <div>
                        <Card text = "dark">
                            <Nav.Link className="city-instance" href={("/" + this.state.type_of_search.split('_')[1] + "/" + instanceTitle)}>
                                <Card.Img variant="top" class="photo" src={search[i].photo}/>
                                <Card.Body>{instanceTitle}</Card.Body>
                                <Card.Title>{str}</Card.Title>
                            </Nav.Link>
                        </Card>
                        <br />
                    </div>
                )
            }
        }
        return elements;
    }

    callSearchQuery = (search_model) => {
        axios.get(`https://api.whereyouwannabe.org/`+ search_model+ `?search_query=` + this.state.query)
            .then(res => {
                const search = res.data;
                this.setState({search});
                this.setState({type_of_search: search_model});
                window.location = "https://www.whereyouwannabe.org/search/" + search_model + "/" + this.state.query
                // window.location = "http://localhost:3000/search/"+ search_model + "/" + this.state.query

            })
    }

    displayText = (city) => {
        let children = [];
        for (var key in city) {
            if (String(city[key]).toLowerCase().includes(this.state.query.toLowerCase()) && !key.includes("photo")) {
                children.push(String(city[key]))

            }

        }
        return children
    };

    render() {
        if (this.state.search === null) {
            return <b> Loading... </b>
        } else {
            return (
                <div>
                    <Navigation/>
                    <div className="btn-group d-flex" role="group" aria-label="...">
                        <button type="button" className="btn btn-primary" id='dropdown-btn' onClick={() => this.callSearchQuery("search_cities")}>Cities
                        </button>
                        <button type="button" className="btn btn-primary" id='dropdown-btn' onClick={() => this.callSearchQuery("search_professions")}>Professions
                        </button>
                        <button type="button" className="btn btn-primary" id='dropdown-btn' onClick={() => this.callSearchQuery("search_universities")}>Universities
                        </button>
                    </div>
                    <body className='background'>
                    <header className="Home-header">
                        <CardDeck className="SearchDeck">
                            <div class="info" align="center">
                                {this.createSearchResultRows(this.state.search)}
                            </div>
                        </CardDeck>
                    </header>
                    </body>
                </div>

            );
        }
    }
}

export default Search;
