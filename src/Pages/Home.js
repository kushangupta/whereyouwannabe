import React, {Component} from 'react';
import '../Styles/Home.css';
import Navigation from '../Components/Navigation';
import cityphoto from '../static_resources/homephoto.jpg';
import Footer from '../Components/Footer';

class Home extends Component {
    render() {
        return (
            <div>
                <Navigation/>
                <body id='mybody' background={cityphoto} className="Home">
                <header className="Home-header">
                    <div className="Title-summary">
                        Start Your Journey to a Better Lifestyle
                        <div className="summary">
                            Moving and starting over can be tough,<br/>
                            but step by step, whether you prioritize the lifestyle,<br/>
                            the career or the city, WhereYouWannaBe <br/>
                            can help you find what you're looking for!<br/>
                        </div>
                    </div>
                </header>
                </body>
                <Footer/>
            </div>
        );
    }
}

export default Home;
