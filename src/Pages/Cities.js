import React, {Component} from 'react';
import Button from 'react-bootstrap/Button'
import Navigation from '../Components/Navigation.js';
import '../Styles/Model.css';
import CardDeck from 'react-bootstrap/CardDeck';
import Card from 'react-bootstrap/Card';
import {Nav} from 'react-bootstrap';
import axios from 'axios';
import CitiesFilterMenu from '../Components/CitiesFilterMenu.js';
import Footer from '../Components/Footer';

import '../Styles/Instance.css';

class Cities extends Component {
    state = {
        cities: null,
        page: 1,
        stateFilterText: "State",
        majorFilterText: "Major",
        apiLink: "https://api.whereyouwannabe.org/cities",
        SortText: "Sort",
        displayedSortText: "Sort",
        searchQuery: "Search",
        search: "search_cities"
    };

    sortMap = {
        "Name": "city_name",
        "Crime Index": "crime_index",
        "Rent Index": "rent_index",
        "Health Care Index": "health_care_index"
    };

    componentDidMount() {
        axios.get(this.state.apiLink)
            .then(res => {
                const cities = res.data;
                this.setState({cities});
            })
    }

    setter = (apiLink) => {
        this.setState({apiLink})
  
        axios.get(this.state.apiLink)
        .then(res => {
            const cities = res.data;
            this.setState({ cities });
        })
    }

    createCityRows = (cities) => {
        let rows = [];
        for (let i = 0; i < 5; i++) {
            let children = [];
            for (let j = 0; j < 3; j++) {
                if (cities.length <= j + 3 * i) {
                    break
                }
                var city = cities[j + (3 * i)];
                children.push(
                    <Card>
                        <Nav.Link className="city-instance" href={("/cities/" + String(city.city_name))}>
                            <Card.Img variant="top" class="photo" src={city.photo}/>
                            <Card.Body>
                                <Card.Title>{city.city_name}</Card.Title>
                                <Card.Text>Crime Index: {city.crime_index}</Card.Text>
                                <Card.Text>Rent Index: {city.rent_index}</Card.Text>
                                <Card.Text>Health Care Index: {city.health_care_index}</Card.Text>
                            </Card.Body>
                        </Nav.Link>
                    </Card>
                )
            }
            rows.push(<br></br>);
            rows.push(<div className="row">{children}</div>);
            if (cities.length < 3 * i) break
        }
        return rows
    };

    handleSubmit = (event, pageNum) => {
        event.preventDefault();

        const page = pageNum;

        if (page != 0) {
            axios.post(this.state.apiLink, {page})
                .then(res => {
                    const cities = res.data;
                    window.scrollTo(0, 0);
                    this.setState({cities});
                    this.setState({page});
                })
        }
    }

    handleButtons = (page) => {
      let buttons = []
      if(page != 1){
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, 1)}}>
            first
          </Button>
        )
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, page - 1)}}>
            &lt;&lt;
          </Button>
        )
      }
      buttons.push(
        <Button variant='primary' id='curr-page' disabled>
        {page}
        </Button>
      )
      if(page != 5){
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, page + 1)}}>
            &gt;&gt;
          </Button>
        )
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, 5)}}>
            last
          </Button>
        )
      }
      return buttons
    }

  render() {
      if (this.state.cities === null) {
          return <b> Loading... </b>
      } else {
          const search = this.state.search
          return (
              <div>
                  <Navigation/>
                  <div class="Model">
                      <header class="Model-header">
                          <h2 className='Cities-header'>Cities</h2>
                          <h5>What type of city are you looking for? <br/>Live music and paddleboarding or quiet
                              suburban living?<br/>
                              Find what's best for you</h5>

                          <div className='row d-flex justify-content-center'>
                            <CitiesFilterMenu setter={this.setter} />
                          </div>

                          <CardDeck className="ModelDeck">
                              {this.createCityRows(this.state.cities)}
                          </CardDeck>
                          {this.handleButtons(this.state.page)}
                        </header>
                    </div>
                    <Footer/>
                </div>
            );
        }
    }
}

export default Cities;

