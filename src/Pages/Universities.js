import React, {Component} from 'react';
import Button from 'react-bootstrap/Button'
import Navigation from '../Components/Navigation.js';
import '../Styles/Model.css';
import '../Styles/Filter.css';
import CardDeck from 'react-bootstrap/CardDeck';
import Card from 'react-bootstrap/Card';
import {ButtonToolbar, Dropdown, Nav} from 'react-bootstrap';
import axios from 'axios';
import UniversitiesFilterMenu from '../Components/UniversitiesFilterMenu.js';
import Footer from '../Components/Footer';

import '../Styles/Instance.css';

class Universities extends Component {
    state = {
        universities: null,
        page: 1,
        stateFilterText: "State",
        topMajorFilterText: "Major",
        apiLink: "https://api.whereyouwannabe.org/universities",
        SortText: "Sort",
        displayedSortText: "Sort",
        search: "search_universities"
    };

    sortMap = {
        "Name": "university_name",
        "Population": "student_size",
        "Admission Rate": "admission_rate",
        "Academic Year Cost": "academic_year_cost"
    };

    componentDidMount() {
        axios.get(this.state.apiLink)
            .then(res => {
                const universities = res.data;
                this.setState({universities});
            })
    }

    createUniversityRows = (universities) => {
        let rows = [];
        for (let i = 0; i < 5; i++) {
            let children = [];
            for (let j = 0; j < 3; j++) {
                if (universities.length <= j + 3 * i) {
                    break
                }
                var uni = universities[j + (3 * i)];
                var admissionRate = Math.round(uni.admission_rate * 100);
                children.push(
                    <Card>
                        <Nav.Link className="university-instance"
                                  href={("/universities/" + String(uni.university_name))}>
                            <Card.Img variant="top" class="photo" src={uni.photo}/>
                            <Card.Body>
                                <Card.Title>{uni.university_name}</Card.Title>
                                <Card.Text>Location: {uni.city_name}, {uni.state_name}</Card.Text>
                                <Card.Text>Top major: {uni.top_major}</Card.Text>
                                <Card.Text>Admission Rate: {admissionRate}%</Card.Text>
                            </Card.Body>
                        </Nav.Link>
                    </Card>
                )
            }
            rows.push(<br></br>);
            rows.push(<div className="row">{children}</div>);
            if (universities.length < 3 * i) break
        }
        return rows
    };

    handleSubmit = (event, pageNum) => {
        event.preventDefault();

        const page = pageNum;

        if (page != 0) {
            axios.post(this.state.apiLink, {page})
                .then(res => {
                    const universities = res.data;
                    window.scrollTo(0, 0);
                    this.setState({universities});
                    this.setState({page});
                })
        }
    };

    setter = (apiLink) => {
        this.setState({apiLink})
  
        axios.get(this.state.apiLink)
        .then(res => {
            const universities = res.data;
            this.setState({ universities });
        })
    }

    handleButtons = (page) => {
      let buttons = []
      if(page != 1){
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, 1)}}>
            first
          </Button>
        )
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, page - 1)}}>
            &lt;&lt;
          </Button>
        )
      }
      buttons.push(
        <Button variant='primary' id='curr-page' disabled>
        {page}
        </Button>
      )
      if(page != 7){
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, page + 1)}}>
            &gt;&gt;
          </Button>
        )
        buttons.push(
          <Button variant="primary" id='page-btn' onClick={(e) => {this.handleSubmit(e, 7)}}>
            last
          </Button>
        )
      }
      return buttons
    }

    render() {
        const search = this.state.search;
        if (this.state.universities === null) {
            return <b> Loading... </b>
        } else {
            return (
                <div>
                    <Navigation/>
                    <div class="Model">
                        <header class="Model-header">
                            <h2 className='Cities-header'>Universities</h2>
                            <h5>Which university are you looking for? <br/>Large & public and or private and liberal
                                arts?<br/>
                                Find what's best for you</h5>
                            <div className='row d-flex justify-content-center'>
                                <UniversitiesFilterMenu setter={this.setter} />
                            </div>

                            <CardDeck className="ModelDeck">
                                {this.createUniversityRows(this.state.universities)}
                            </CardDeck>
                            {this.handleButtons(this.state.page)}
                        </header>
                    </div>
                    <Footer/>
                </div>
            );
        }
    }
}

export default Universities;
