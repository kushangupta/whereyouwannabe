import React, {Component} from 'react';
import CardDeck from 'react-bootstrap/CardDeck'
import Card from 'react-bootstrap/Card'
import Navigation from '../Components/Navigation.js'
import GitlabTable from '../Components/GitlabTable.js'
import BryceFace from '../static_resources/BryceFace.jpg';
import AlexFace from '../static_resources/Alexface.jpg';
import KushanFace from '../static_resources/kushanface.jpg';
import KarenFace from '../static_resources/karenface.png';
import LisaFace from '../static_resources/lisaface.jpg';
import Postman from '../static_resources/Postman.jpg';
import AWS from '../static_resources/AWS.jpg';
import Python from '../static_resources/Python.jpg';
import ReactPic from '../static_resources/React.jpg';
import SQLA from '../static_resources/SQLAlchemy.jpg';
import Selenium from '../static_resources/Selenium.jpg';
import GitLab from '../static_resources/GitLab.jpg';
import Jest from '../static_resources/Jest.jpg';
import Postgresql from '../static_resources/Postgresql.jpg';
import unittest from '../static_resources/unittest.jpg';
import Flask from '../static_resources/Flask.jpg';
import DB from '../static_resources/DBBrowser.jpg';
import '../Styles/Model.css';

class About extends Component {
    render() {
        return (
            <div>
                <Navigation/>
                <div className="About">
                    <h1 className="Model-header">
                        about us
                    </h1>
                    <h5 className="center">Project Goal: </h5>
                    <p align="center" className="pAbout">
                        An individual’s well-being is highly dependent on how well the person fits in with the location
                        they reside in.
                        <br/>
                        There are many crucial factors that someone considers when analyzing potential places to
                        relocate to.
                        <br/>
                        Thus, we decided to create a web application that streamlines those crucial deciding data points
                        that people
                        factor into their moving decisions.
                        <br/>
                        Our website, WhereYouWannaBe.org, provides information regarding facts and
                        details about particular cities, various professions and their prevalence within particular
                        cities,
                        and the universities within those cities and the top careers that arise from those institutions.
                    </p>
                    <br/>
                    <h5 className="center">Data Integration Insight: </h5>
                    <p align="center" className="pAbout">
                        We integrated the disparate data of cities, professions, and universities together to showcase
                        how these data points
                        are all equally important when considering a place to relocate to. There were sometimes
                        disrepancies between datasets
                        such as dealing with linking professions with cities and universities with professions. However,
                        each dataset used for
                        the models contained information that would link each model to the other ones which highlighted
                        that although
                        the information was presented in a disparate manner, they were still related and were compatible
                        as linked models.
                    </p>

                    <CardDeck className="AboutBlocks">
                        <Card>
                            <Card.Img variant="top" src={BryceFace}/>
                            <Card.Body>
                                <Card.Title>Bryce Dai</Card.Title>
                                <Card.Text>
                                    Ricey Bryce
                                </Card.Text>
                                <footer className="blockquote-footer">
                                    Responsibilities: Full Stack
                                </footer>
                                <footer className="blockquote-footer">
                                    I have no hobbies anymore. My life is computer science now.
                                </footer>
                            </Card.Body>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src={KarenFace}/>
                            <Card.Body>
                                <Card.Title>Karen Ding</Card.Title>
                                <Card.Text>
                                    Kant Kut Kit Karen
                                </Card.Text>
                                <footer className="blockquote-footer">
                                    Responsibilities:
                                    Full Stack
                                </footer>
                                <footer className="blockquote-footer">
                                    An active member of EPIC. Loves Netflix, Boba, and Smash Bros.
                                </footer>
                            </Card.Body>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src={KushanFace}/>
                            <Card.Body>
                                <Card.Title>Kushan Gupta</Card.Title>
                                <Card.Text>
                                    A God.
                                </Card.Text>
                                <footer className="blockquote-footer">
                                    Responsibilities:
                                    Full Stack
                                </footer>
                                <footer className="blockquote-footer">
                                    Loves Urban Dance, food, and photography.
                                </footer>
                            </Card.Body>
                        </Card>
                        <Card>
                            <Card.Img variant="top" src={AlexFace}/>
                            <Card.Body>
                                <Card.Title>Alex Zhao</Card.Title>
                                <Card.Text>
                                    Zip zip
                                </Card.Text>
                                <footer className="blockquote-footer">
                                    Responsibilities:
                                    Full Stack
                                </footer>
                                <footer className="blockquote-footer">
                                    Find him at the racquetball courts as well as at Cabo Bobs.
                                </footer>
                            </Card.Body>
                        </Card> <Card>
                        <Card.Img variant="top" src={LisaFace}/>
                        <Card.Body>
                            <Card.Title>Lisa Yu</Card.Title>
                            <Card.Text>
                                Le Sneaker
                            </Card.Text>
                            <footer className="blockquote-footer">
                                Responsibilities:
                                Full Stack
                            </footer>
                            <footer className="blockquote-footer">
                                Collects shoes, drinks boba, and curates Spotify playlists.
                            </footer>
                        </Card.Body>
                    </Card>
                    </CardDeck>

                </div>
                <GitlabTable/>
                <br/>
                <h1 className="center">
                    Tools Used
                </h1>
                <CardDeck className="ToolBlocks">
                    <Card>
                        <Card.Img variant="top" src={AWS}/>
                        <Card.Body>
                            <Card.Title>
                                Amazon Web Services
                            </Card.Title>
                            <Card.Text>
                                Used to host website backend and frontend.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={Python}/>
                        <Card.Body>
                            <Card.Title>
                                Python
                            </Card.Title>
                            <Card.Text>
                                Used to create our server.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={ReactPic}/>
                        <Card.Body>
                            <Card.Title>
                                React.js
                            </Card.Title>
                            <Card.Text>
                                Used to create the front-end.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={SQLA}/>
                        <Card.Body>
                            <Card.Title>
                                SQLAlchemy
                            </Card.Title>
                            <Card.Text>
                                Used as the ORM to query the DB in the backend.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={Postman}/>
                        <Card.Body>
                            <Card.Title>
                                Postman
                            </Card.Title>
                            <Card.Text>
                                Used to design API.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={Selenium}/>
                        <Card.Body>
                            <Card.Title>
                                Selenium
                            </Card.Title>
                            <Card.Text>
                                Used to test UI elements interactivity.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </CardDeck>
                <CardDeck className="ToolBlocks">
                    <Card>
                        <Card.Img variant="top" src={GitLab}/>
                        <Card.Body>
                            <Card.Title>
                                GitLab
                            </Card.Title>
                            <Card.Text>
                                Used to store files for team collaboration.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={Jest}/>
                        <Card.Body>
                            <Card.Title>
                                Jest
                            </Card.Title>
                            <Card.Text>
                                Used to test JavaScript files.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={Postgresql}/>
                        <Card.Body>
                            <Card.Title>
                                PostgreSQL
                            </Card.Title>
                            <Card.Text>
                                Used for database type.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={unittest}/>
                        <Card.Body>
                            <Card.Title>
                                unittest
                            </Card.Title>
                            <Card.Text>
                                Used to test individual functions in server.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={Flask}/>
                        <Card.Body>
                            <Card.Title>
                                Flask
                            </Card.Title>
                            <Card.Text>
                                Used to create backend server/API.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card>
                        <Card.Img variant="top" src={DB}/>
                        <Card.Body>
                            <Card.Title>
                                DB Browser for SQLite
                            </Card.Title>
                            <Card.Text>
                                Used to search and modify DB manually.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </CardDeck>
                <br/>
                <br/>
                <h1 className="center">
                    Data sources
                </h1>
                <h5 className="center"><a href="https://www.numbeo.com/common/api.jsp" className="h5"> Cities API </a>
                </h5>
                <p align="center" className="pAbout">
                    Information and metrics about cities within the United States. We ran an API call and used/parsed
                    the JSON information that was returned.
                </p>
                <h5 className="center"><a
                    href="https://api.careeronestop.org/api-explorer/home/index/Occupations_GetOccupationDetails"
                    className="h5"> Professions API </a></h5>
                <p align="center" className="pAbout">
                    Information from the Department of Labor regarding professions and occupations and the count for
                    each particular job within the United States. We ran an API call and used/parsed the JSON
                    information that was returned.
                </p>
                <h5 className="center"><a href="https://collegescorecard.ed.gov/data/documentation/"
                                          className="h5"> Universities API </a></h5>
                <p align="center" className="pAbout">
                    Information and demographics from The Integrated Postsecondary Education Data System regarding
                    accredited universities in the United States. We ran an API call and used/parsed the JSON
                    information that was returned.
                </p>
                <h5 className="center"><a href="https://api.cognitive.microsoft.com/bing/v7.0/images/search?q="
                                          className="h5"> Photos API </a></h5>
                <p align="center" className="pAbout">
                    High-quality photos for the model instances. We ran an API call and a photo link was returned based
                    on the inputted parameter.
                </p>
                <h1 className="center">
                    Links
                </h1>
                <h5 className="center"><a href="https://gitlab.com/kushangupta/whereyouwannabe" className="h5"> GitLab
                    Repository </a></h5>
                <h5 className="center"><a href="https://documenter.getpostman.com/view/7263584/SVtR1AA3?version=latest"
                                          className="h5"> Postman API Documentation </a></h5>
            </div>
        );
    }
}

export default About;
