import React from 'react';
import { Nav, Navbar, Form, FormControl, Button } from 'react-bootstrap';
import logo from "../static_resources/logo.png";
import '../Styles/Navigation.css';

class Navigation extends React.Component {
	render() {
		return (
			<Navbar className="Navme" expand="lg">
			  <Navbar.Brand href="/"><img src= { logo } alt="" width="20%" height="20%"></img> WhereYouWannaBe</Navbar.Brand>
			  <Navbar.Toggle aria-controls="basic-navbar-nav" />
			  <Navbar.Collapse id="basic-navbar-nav">
			    <Nav className="mr-auto">
			      <Nav.Link href="/cities">Cities</Nav.Link>
			      <Nav.Link href="/professions">Professions</Nav.Link>
			      <Nav.Link href="/col">Cost of living</Nav.Link>
			      <Nav.Link href="/about">About</Nav.Link>
			    </Nav>
			    <Form inline>
			      <FormControl type="text" placeholder="Search" className="mr-sm-2" />
			      <Button id="searchbutn">Search</Button>
			    </Form>
			  </Navbar.Collapse>
			</Navbar>
			)
	}
}
ReactDOM.render(<Navigation/>, document.getElementById('root'));
export default Navigation;
