import React, {Component} from "react";
import BChart from '@weknow/react-bubble-chart-d3';
import axios from "axios";

import '../Styles/Model.css';

class CustomerBubbleChart extends Component {
    state = {
        data: {}
    }

    componentDidMount() {
        this.createData()
    }

    createData() {
        axios.get('https://www.stockoverflow.me/api/stock')
            .then(res => {
                const stocks = res.data['objects'];
                let data = []
                var range;
                let ranges = ['0 to 10',
                    '10 to 25',
                    '25 to 50',
                    '51 to 200',
                    '201 to 500',]
                for (range in ranges) {
                    let ran = {}
                    var stock;
                    ran['label'] = ranges[range]
                    ran['value'] = 0
                    for (stock in stocks) {
                        var price = stocks[stock]['current_price']
                        if (range == 0) {
                            if (price>=0 && price<=10) {
                                ran['value'] += 1
                            }
                        }
                        if (range == 1) {
                            if (price>=11 && price<=25) {
                                ran['value'] += 1
                            }
                        }
                        if (range == 2) {
                            if (price>=26 && price<=50) {
                                ran['value'] += 1
                            }
                        }
                        if (range == 3) {
                            if (price>=51 && price<=200) {
                                ran['value'] += 1
                            }
                        }
                        if (range == 4) {
                            if (price>=201 && price<=500) {
                                ran['value'] += 1
                            }
                        }
                    }
                    data.push(ran)
                }
                this.setState({data});

            })

    }

    render() {
        if (this.state.data.length == null) {
            return <b></b>
        }
        return (
            <div>
                <h4 className='Viz-header'>Displays number of stocks within certain price range</h4>
            <BChart
                graph={{
                    zoom: .9,
                    offsetX: .04,
                    offsetY: .05,
                }}
                width={1200}
                height={1000}
                padding={0} // optional value, number that set the padding between bubbles
                showLegend={true} // optional value, pass false to disable the legend.
                legendPercentage={35} // number that represent the % of with that legend going to use.
                legendFont={{
                    family: 'Arial',
                    size: 12,
                    color: '#000',
                    weight: 'bold',
                }}
                valueFont={{
                    family: 'Arial',
                    size: 18,
                    color: '#fff',
                    weight: 'bold',
                }}
                labelFont={{
                    family: 'Arial',
                    size: 0,
                    color: '#fff',
                    weight: 'bold',
                }}

                bubbleClickFunc={this.bubbleClick}
                legendClickFun={this.legendClick}

                data={this.state.data}
            />
            </div>
        )

    }
}

export default CustomerBubbleChart;