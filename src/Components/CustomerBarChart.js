import React from 'react';
import {Element} from "react-faux-dom";
import '../Styles/Model.css';
import * as d3 from "d3";
import '../Styles/CitiesBarGraph.css';
import axios from 'axios';

class CustomerBarChart extends React.Component {
    state = {
        indexData: [],
    };

    getIndexData() {
        let data = [];
        let indexNames = [];
        axios.get('https://www.stockoverflow.me/api/index')
            .then(res => {
                const data = res.data["objects"];
                //console.log(data);
                for (let index = 0; index < data.length / 2; index++) {
                    //console.log(data[index]);
                    let name = data[index]["name"];
                    indexNames.push(name);
                    let dict = {}
                    dict[name] = data[index]["stocks"].length;
                    data.push(dict);
                }
                data.sort(function (a, b) {
                    var keyA = Object.keys(a)[0];
                    var keyB = Object.keys(b)[0];

                    if (keyA > keyB) {
                        return 1;
                    }
                    if (keyB > keyA) {
                        return -1;
                    }
                    return 0;
                });
                this.setState({indexData: data})
                //console.log(this.state.indexData);
            })
    }

    componentDidMount() {
        this.getIndexData()
    }

    render() {
        if (this.state.indexData.length == 0) {
            return <b> Loading... </b>
        } else {
            let width = 1200;
            let height = 600;

            const el = new Element("div");
            const svg = d3
                .select(el)
                .append("svg")
                .attr("id", "chart")
                .attr("width", width)
                .attr("height", height);

            const margin = {
                top: 60,
                bottom: 100,
                left: 80,
                right: 40
            };

            const chart = svg
                .append("g")
                .classed("display", true)
                .attr("transform", `translate(${margin.left},${margin.top})`);

            width = width - margin.left - margin.right;
            height = height - margin.top - margin.bottom;

            var data = this.state.indexData;
            var x = "Index";
            var y = "Number of Stocks";

            // creating scales
            const xScale = d3
                .scaleBand()
                .domain(data.map(d => Object.keys(d)[0]))
                .range([0, width]);
            const yScale = d3
                .scaleLinear()
                .domain([0, d3.max(data, d => Object.values(d)[0])])
                .range([height, 0]);
            const colorScale = d3.scaleSequential(d3.interpolateBlues);

            chart
                .selectAll(".bar-label")
                .data(data)
                .enter()
                .append("text")
                .classed("bar-label", true)
                .attr("x", d => xScale(Object.keys(d)[0]) + xScale.bandwidth() / 2)
                .attr("dx", 0)
                .attr("y", d => yScale(Object.values(d)[0]))
                .attr("dy", -6)

            const xAxis = d3.axisBottom().scale(xScale);
            chart
                .append("g")
                .classed("x axis", true)
                .attr("transform", `translate(0,${height})`)
                .call(xAxis);

            const yAxis = d3
                .axisLeft()
                .ticks(5)
                .scale(yScale);

            chart
                .append("g")
                .classed("y axis", true)
                .attr("transform", "translate(0,0)")
                .call(yAxis);

            chart
                .select(".x.axis")
                .append("text")
                .attr("x", width / 2)
                .attr("y", 60)
                .attr("fill", "#000")
                .style("font-size", "20px")
                .style("text-anchor", "middle")
                .text(x);

            chart
                .select(".y.axis")
                .append("text")
                .attr("x", 0)
                .attr("y", 0)
                .attr("transform", `translate(-50, ${height / 2}) rotate(-90)`)
                .attr("fill", "#000")
                .style("font-size", "20px")
                .style("text-anchor", "middle")
                .text(y);

            const yGridlines = d3
                .axisLeft()
                .scale(yScale)
                .ticks(5)
                .tickSize(-width, 0, 0)
                .tickFormat("");

            chart
                .append("g")
                .call(yGridlines)
                .classed("gridline", true);

            chart
                .selectAll(".bar")
                .data(data)
                .enter()
                .append("rect")
                .classed("bar", true)
                .attr("x", d => xScale(Object.keys(d)[0]))
                .attr("y", d => yScale(Object.values(d)[0]))
                .attr("height", d => height - yScale(Object.values(d)[0]))
                .attr("width", d => xScale.bandwidth() - 5)
                .style("fill", (d, i) => colorScale(0.8));

            return (
                <div>
                    <h4 className='Viz-header'>Displays number of stocks within an index</h4>
                    {el.toReact()}
                </div>
            );
        }
    }
}

export default CustomerBarChart;