import React from 'react';
import {Button, Form, FormControl} from 'react-bootstrap';

class SearchBar extends React.Component {

    state = {
        inputNode: "Search",
        search: "search_cities"
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.search) {
            this.setState({
                search: this.props.search
            })
        }
    }

    handleKeyPress(target) {
        if (target.charCode == 13) {
            target.preventDefault();
            window.location.href = ("/search/" + String(this.state.search) + "/" + String(this.state.inputNode))
        }
    }

    render() {
        const search = this.state.search;
        return (
            <Form inline>
                <FormControl id="search-box" type="text" placeholder={this.state.inputNode} className="mr-sm-2"
                             onChange={node => this.setState({inputNode: node.target.value})}
                             onKeyPress={key => {
                                 this.handleKeyPress(key)
                             }}
                             search={search}
                />
                <Button id="searchbutn"
                        href={("/search/" + String(this.state.search) + "/" + String(this.state.inputNode))}
                        search={search}>Search</Button>
            </Form>
        )
    }
}

export default SearchBar;
