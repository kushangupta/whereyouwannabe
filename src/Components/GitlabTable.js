import React from 'react';
import Table from 'react-bootstrap/Table'
import '../Styles/Model.css';

// import { Nav, Navbar, Form, FormControl, Button } from 'react-bootstrap';

var commitMap;
var issueMap;

class GitlabTable extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            commitsArray: [],
            issuesArray: []
        };
    }

    componentDidMount() {
        // go through multiple iterations incase all the commits information can't be displayed on one page
        for (let page = 1; page <= 10; page++) {
            // retrieve and set the state variable for commits array
            fetch('https://gitlab.com/api/v4/projects/14464209/repository/commits?per_page=100&page=' + page, {
                method: 'get', //Get / POST / ...
            }).then(response => {
                return response.json();
            })
                .then(jsonArray => {
                    // make sure the commits array exists
                    if (typeof this.state.commitsArray == "undefined") {
                        this.state.commitsArray = []
                    }
                    // merge existing commits array with new commits information in jsonArray
                    this.state.commitsArray = this.state.commitsArray.concat(jsonArray)
                })
                .catch(console.log("Error in retrieving commits from Gitlab"));
        }


        // retrieve and set the state for issues array
        fetch('https://gitlab.com/api/v4/projects/14464209/issues?per_page=100&page=1', {
            method: 'get', //Get / POST / ...
        }).then(response => {
            return response.json();
        })
        .then(jsonArray => {
            this.setState({issuesArray: jsonArray});
        })
        .catch(console.log("Error in retrieving issues from Gitlab"));
    }

    renderTableData() {
        return this.state.students.map((student) => {
            const {name, commits, issues, tests} = student;
            return (
                <tr key={name}>
                    <td>{name}</td>
                    <td>{commits}</td>
                    <td>{issues}</td>
                    <td>{tests}</td>
                </tr>
            )
        })
    }

    render() {
        // keep track of commits and issues for each member
        commitMap = {
            "Bryce Dai": 0,
            "lisa-yuLisa Yu": 0,
            "Kayrhen": 0,
            "Kushangupta": 0,
            "SyngericalAlex Zhao": 0,
            "Total": 0
        };
        issueMap = {"Bryce Dai": 0, "Lisa Yu": 0, "Karen Ding": 0, "Kushan Gupta": 0, "Alex Zhao": 0, "Total": 0};

        // populate commitMap with everyone's commit numbers
        for (let commit in this.state.commitsArray) {
            let committer_name = this.state.commitsArray[commit]["committer_name"];
            // only add to map if it already exists, incase there is another unexpected committer
            for (var name in commitMap) {
                if (name.includes(committer_name)) {
                    commitMap[name] = commitMap[name] + 1;
                    commitMap["Total"] = commitMap["Total"] + 1
                }
            }
        }

        // populate issueMap with everyone's number of closed issues
        for (let issue in this.state.issuesArray) {
            let closed_by = this.state.issuesArray[issue]["closed_by"];
            // if the issue was not null, meaning it was actually closed
            if (closed_by != null) {
                let name = this.state.issuesArray[issue]["closed_by"]["name"];
                // only add to map if it already exists, incase there is another unexpected issue closer
                if (issueMap.hasOwnProperty(name)) {
                    issueMap[name] = issueMap[name] + 1;
                    issueMap["Total"] = issueMap["Total"] + 1;
                }
            }
        }

        // set the state variable for the table actually displayed on the about page
        this.state = {
            students: [
                {name: 'Bryce Dai', commits: commitMap["Bryce Dai"], issues: issueMap["Bryce Dai"], tests: '8'},
                {name: 'Karen Ding', commits: commitMap["Kayrhen"], issues: issueMap["Karen Ding"], tests: '6'},
                {name: 'Kushan Gupta', commits: commitMap["Kushangupta"], issues: issueMap["Kushan Gupta"], tests: '7'},
                {name: 'Lisa Yu', commits: commitMap["lisa-yuLisa Yu"], issues: issueMap["Lisa Yu"], tests: '9'},
                {
                    name: 'Alex Zhao',
                    commits: commitMap["SyngericalAlex Zhao"],
                    issues: issueMap["Alex Zhao"],
                    tests: '14'
                },
                {name: 'Total', commits: commitMap["Total"], issues: issueMap["Total"], tests: '44'}
            ]
        };


        return (
            <div className="AboutBlocks">
                <h1 className="center">
                    GitLab Statistics
                </h1>
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Commits</th>
                        <th>Issues</th>
                        <th>Tests</th>
                    </tr>
                    </thead>
                    {this.renderTableData()}
                </Table>
            </div>
        );
    }
}

export default GitlabTable;