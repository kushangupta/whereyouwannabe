import React from 'react';
import {Nav, Navbar} from 'react-bootstrap';
import logo from "../static_resources/logo.png";
import '../Styles/Navigation.css';
import SearchBar from '../Components/SearchBar.js';

class Navigation extends React.Component {
    state = {
        inputNode: "Austin"
    };

    render() {
        if (this.state.inputNode === null) {
            return (<b>loading</b>)
        } else {
            return (
                <Navbar className="Navme" expand="lg">
                    <Navbar.Brand href="/"><img src={logo} alt="" width="20%"
                                                height="20%"></img> WhereYouWannaBe</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link href="/cities">Cities</Nav.Link>
                            <Nav.Link href="/professions">Professions</Nav.Link>
                            <Nav.Link href="/universities">Universities</Nav.Link>
                            <Nav.Link href="/about">About</Nav.Link>
                            <Nav.Link href="/api">API</Nav.Link>
                            <Nav.Link href="/visualizations">Visualizations</Nav.Link>
                        </Nav>
                        <SearchBar/>
                    </Navbar.Collapse>
                </Navbar>
            )
        }
    }
}

export default Navigation;
