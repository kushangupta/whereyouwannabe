import React, {Component} from 'react';
import {ButtonToolbar, Dropdown, Nav} from 'react-bootstrap';
import Button from 'react-bootstrap/Button'
import axios from 'axios';
import SearchBar from './SearchBar.js';


class UniversitiesFilterMenu extends React.Component {
	state = {
		universities: null,
        page: 1,
        stateFilterText: "State",
        topMajorFilterText: "Major",
        apiLink: "https://api.whereyouwannabe.org/universities",
        SortText: "Sort",
        displayedSortText: "Sort",
        search: "search_universities"
	}

	sortMap = {
        "Name": "university_name",
        "Population": "student_size",
        "Admission Rate": "admission_rate",
        "Academic Year Cost": "academic_year_cost"
    };

	constructor(props) {
        super(props);
    }

	render() {
		const search = this.state.search;

		return (
			<ButtonToolbar>
				<SearchBar search={search}/>
				<Dropdown className='m-1' id='dropdown-btn'>
					<Dropdown.Toggle variant="success" id="dropdown-basic">
						{this.state.stateFilterText}
					</Dropdown.Toggle>
					<Dropdown.Menu className='ourdropdown'>
						{["All", "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"].map(value => {
							return (
								<Dropdown.Item
									key={value}
									onClick={() => {
										this.setState({stateFilterText: value});
									}}
								>
									{value}
								</Dropdown.Item>
							);
						})}
					</Dropdown.Menu>
				</Dropdown>

				<Dropdown className='m-1' id='dropdown-btn'>
					<Dropdown.Toggle variant="success" id="dropdown-basic">
						{this.state.topMajorFilterText}
					</Dropdown.Toggle>
					<Dropdown.Menu className='ourdropdown'>
						{["All", "communication", "engineering", "biological", "computer", "humanities", "health", "psychology", "mathematcs", "multidiscipline"].map(value => {
							return (
								<Dropdown.Item
									key={value}
									onClick={() => {
										this.setState({topMajorFilterText: value});
									}}
								>
									{value}
								</Dropdown.Item>
							);
						})}
					</Dropdown.Menu>
				</Dropdown>

				<Dropdown className='m-1' id='dropdown-btn'>
					<Dropdown.Toggle variant="success" id="dropdown-basic">
						{this.state.displayedSortText}
					</Dropdown.Toggle>
					<Dropdown.Menu className='ourdropdown'>
						{["All", "Name", "Population", "Admission Rate", "Academic Year Cost"].map(value => {
							return (
								<Dropdown.Item
									key={value}
									onClick={() => {
										this.setState({
											SortText: this.sortMap[value],
											displayedSortText: value
										});
									}}
								>
									{value}
								</Dropdown.Item>
							);
						})}
					</Dropdown.Menu>
				</Dropdown>

				<Button className='m-1' id='go-btn'
						onClick={() => {
							axios.get(`https://api.whereyouwannabe.org/universities/filter?state=` + this.state.stateFilterText + "&top_major=" + this.state.topMajorFilterText + "&sort=" + this.state.SortText)
								.then(res => {
									const universities = res.data;
									this.setState({universities});
									this.setState({page: 1});
									this.props.setter(`https://api.whereyouwannabe.org/universities/filter?state=` + this.state.stateFilterText + "&top_major=" + this.state.topMajorFilterText + "&sort=" + this.state.SortText);
								})
						}}
				>
					Go
				</Button>
				</ButtonToolbar>
		);
	}
}

export default UniversitiesFilterMenu;
