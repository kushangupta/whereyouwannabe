import React, {Component} from 'react';
import {ButtonToolbar, Dropdown, Nav} from 'react-bootstrap';
import Button from 'react-bootstrap/Button'
import axios from 'axios';
import SearchBar from './SearchBar.js';


class ProfessionsFilterMenu extends React.Component {
	state = {
		professions: null,
        page: 1,
        educationFilterText: "Education",
        salaryFilterText: "Annual Salary",
        apiLink: "https://api.whereyouwannabe.org/professions",
        SortText: "Sort",
        displayedSortText: "Sort",
        search: "search_professions"
	}

	sortMap = {
        "Name": "profession_name",
        "Median Wage": "profession_median_wage",
        "Projection": "profession_projection"
    };

	constructor(props) {
        super(props);
    }

	render() {
		const search = this.state.search;

		return (
			<ButtonToolbar>
				<SearchBar search={search}/>
				<Dropdown className='m-1' id='dropdown-btn'>
					<Dropdown.Toggle variant="success" id="dropdown-basic">
						{this.state.educationFilterText}
					</Dropdown.Toggle>
					<Dropdown.Menu className='ourdropdown' id='dropdown-options'>
						{["All", "Bachelor's degree", "Doctoral or professional degree", "High school diploma or equivalent", "Some college, no degree", "Master's degree", "Associate's degree", "Postsecondary certificate", "No formal educational credential"].map(value => {
							return (
								<Dropdown.Item
									key={value}
									onClick={() => {
										this.setState({educationFilterText: value});
									}}
								>
									{value}
								</Dropdown.Item>
							);
						})}
					</Dropdown.Menu>
				</Dropdown>

				<Dropdown className='m-1' id='dropdown-btn'>
					<Dropdown.Toggle variant="success" id="dropdown-basic">
						{this.state.salaryFilterText}
					</Dropdown.Toggle>
					<Dropdown.Menu className='ourdropdown' id='dropdown-options'>
						{["All", "$0 - $30,000", "$30,000 - $70,000", "$70,000 - $100,000", "$100,000 - $150,000"].map(value => {
							return (
								<Dropdown.Item
									key={value}
									onClick={() => {
										this.setState({salaryFilterText: value});
									}}
								>
									{value}
								</Dropdown.Item>
							);
						})}
					</Dropdown.Menu>
				</Dropdown>

				<Dropdown className='m-1' id='dropdown-btn'>
					<Dropdown.Toggle variant="success" id="dropdown-basic">
						{this.state.displayedSortText}
					</Dropdown.Toggle>
					<Dropdown.Menu className='ourdropdown' id='dropdown-options'>
						{["All", "Name", "Median Wage", "Projection"].map(value => {
							return (
								<Dropdown.Item
									key={value}
									onClick={() => {
										this.setState({
											SortText: this.sortMap[value],
											displayedSortText: value
										});
									}}
								>
									{value}
								</Dropdown.Item>
							);
						})}
					</Dropdown.Menu>
				</Dropdown>

			<Button className='m-1' id='go-btn'
					onClick={() => {
						axios.get("https://api.whereyouwannabe.org/professions/filter?education=" + this.state.educationFilterText + "&annual_salary=" + this.state.salaryFilterText + "&sort=" + this.state.SortText)
							.then(res => {
								const professions = res.data;
								this.setState({professions});
								this.setState({page: 1});
								this.props.setter("https://api.whereyouwannabe.org/professions/filter?education=" + this.state.educationFilterText + "&annual_salary=" + this.state.salaryFilterText + "&sort=" + this.state.SortText);
							})
					}}
			>
				Go
			</Button>
			</ButtonToolbar>
		);
	}
}

export default ProfessionsFilterMenu;
