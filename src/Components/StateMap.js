import React, {Component} from 'react';
import * as d3 from "d3";
import stateData from './StateData';
import '../Styles/StatesMap.css';
import axios from "axios";

class StateMap extends Component {
    state = {
            status: null,
            states: null
    };

    constructor(props) {
        super(props);
        this.drawChart = this.drawChart.bind(this);
    }

    componentDidMount() {
        this.createData()
    }

    createData() {
        axios.get('https://api.whereyouwannabe.org/num_universities')
            .then(res => {
                const states = res.data;
                this.setState({states});
            })

    }

    tooltipHtml(n, d){	/* function to create html content string in tooltip div. */
		return "<h4>"+n+"</h4><table>"+
			"<tr><td># Universities</td><td>"+(d.low)+"</td></tr>"+
			"</table>";
	}

    drawChart() {
        var states = this.state.states
        console.log((states))
        for (var state in states){
            if (state == null) {
                states[state] = 0
            }
        }

        var sampleData = {};
        ["HI", "AK", "FL", "SC", "GA", "AL", "NC", "TN", "RI", "CT", "MA",
            "ME", "NH", "VT", "NY", "NJ", "PA", "DE", "MD", "WV", "KY", "OH",
            "MI", "WY", "MT", "ID", "WA", "DC", "TX", "CA", "AZ", "NV", "UT",
            "CO", "NM", "OR", "ND", "SD", "NE", "IA", "MS", "IN", "IL", "MN",
            "WI", "MO", "AR", "OK", "KS", "LA", "VA"]
            .forEach(function (d) {
                // var p = parkStateDict[d];
                // var o = orgStateDict[d];
                // var e = eventStateDict[d];
                var num = 0
                if (states[d] != null) {
                    num = states[d]
                }
                sampleData[d] = {
                    // parks: p, orgs: o, events: e,
                    color: d3.interpolate("#ffffcc", "#307318")(num/5),
                    low: num,
                };
            });

        /* draw states on id #statesvg */
        stateData.draw("#statesvg", sampleData, this.tooltipHtml);

        d3.select(window.frameElement).style("height", "600px");
    }

    render() {
        if (this.state.states != null) {
            this.drawChart();
        }
        return (
            <div className="container">
                <div id="tooltip"></div>
                <svg width="960" height="600" id="statesvg"></svg>
            </div>
        );

    }
}

export default StateMap;
