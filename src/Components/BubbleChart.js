import {Component} from "react";
import Search from "../Pages/Search";
import BChart from '@weknow/react-bubble-chart-d3';
import React from 'react'
import axios from "axios";

import '../Styles/Model.css';

class BubbleChart extends Component {
    state = {
        professions: {}
    }

    componentDidMount() {
        this.createData()
    }

    createData() {
        axios.get('https://api.whereyouwannabe.org/all_professions')
            .then(res => {
                const professions = res.data;
                // var toUse = this.spliceDict(professions, 0, 20)
                this.setState({professions});

            })

    }

    render() {
        if (this.state.professions.length == null) {
            return <b>Loading...</b>
        }
        return (
            <BChart
                graph={{
                    zoom: .9,
                    offsetX: .04,
                    offsetY: .05,
                }}
                width={1200}
                height={1000}
                padding={0} // optional value, number that set the padding between bubbles
                showLegend={true} // optional value, pass false to disable the legend.
                legendPercentage={35} // number that represent the % of with that legend going to use.
                legendFont={{
                    family: 'Arial',
                    size: 12,
                    color: '#000',
                    weight: 'bold',
                }}
                valueFont={{
                    family: 'Arial',
                    size: 18,
                    color: '#fff',
                    weight: 'bold',
                }}
                labelFont={{
                    family: 'Arial',
                    size: 0,
                    color: '#fff',
                    weight: 'bold',
                }}

                bubbleClickFunc={this.bubbleClick}
                legendClickFun={this.legendClick}

                data = {this.state.professions}
            />
        )

    }
}

export default BubbleChart;
