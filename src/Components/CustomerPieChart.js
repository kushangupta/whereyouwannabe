import React from 'react';
import { Element } from "react-faux-dom";
import '../Styles/Model.css';
import * as d3 from "d3";
import '../Styles/Model.css';
import axios from 'axios';
import Slice from './slice.js';

class CustomerPieChart extends React.Component {
    state = {
        pieChartData : [],
        pieChartNames : [],
    };

    getPieChartData() {
        let data = [];
        let names = [];
        let dict = {};

        axios.get('https://www.stockoverflow.me/api/stock')
            .then(res => {
                let objects = res.data["objects"];
                //console.log(data);
                for(let index = 0; index < objects.length; index++) {
                    // get the industry name for this object
                    let industry = objects[index]["industry"]["human_name"]
                    // create entry if first time
                    if(!(industry in dict)) {
                        dict[industry] = 0;
                    }
                    // increment counter for that industry in the dict
                    dict[industry] = dict[industry] + 1;
                }

                for (let key in dict) {
                    if(dict.hasOwnProperty(key)) {
                        names.push(key);
                    }
                }

                for(let index = 0; index < names.length; index++) {
                    let numTimes = dict[names[index]];
                    data.push(numTimes);
                    // if(numTimes > 500) {
                    //     data.push(numTimes);
                    // } else {
                    //     names.splice(index, 1);
                    // }
                }

                this.setState({pieChartData : data});
                this.setState({pieChartNames : names});
            })
    }

    componentDidMount() {
        this.getPieChartData()
    }

    componentDidUpdate() {
        if (this.state.pieChartData != 0) {
          const svg = d3.select("#pieChart");
          const dimensions = svg.node().getBoundingClientRect();
          svg.attr("viewBox",
            `${-dimensions.width / 2} ${-dimensions.height / 2} ${dimensions.width} ${dimensions.height}`);
        }
      }
    

    render() {
        if(this.state.pieChartData.length == 0) {
            return <b> Loading... </b>
        }
        else {
            var data = this.state.pieChartData;
            var arcs = d3.pie()(data);
            var innerRadius = 0;
            var outerRadius = 250;
            return (
                <div>
                    <h4 className='Viz-header'>Number of stocks within each industry</h4>
                <div style={{background: "whitesmoke", paddingBottom: '10px', borderRadius: '10px', marginTop: '50px'}}>
                <h3>Number of Stocks in Each Industry</h3>
                <svg width="750" height="500" id="pieChart" style={{"paddingTop": '10px'}}>
                    {arcs.map((obj, i) =>
                    <Slice
                        key={"slice" + i}
                        innerRadius = {innerRadius}
                        outerRadius = {outerRadius}
                        startAngle = {obj.startAngle}
                        endAngle = {obj.endAngle}
                        fillColor = {d3.rgb(Math.random() * (180 - 1) + 1,
                                    Math.random() * (180 - 1) + 1, Math.random() * (180 - 60) + 1)}
                    />
                    )}
                    {arcs.map((obj, i) => 
                    <text key={"label" + i}
                            className="slice-text"
                            transform={`translate(${d3.arc()
                            .centroid({
                                innerRadius: outerRadius / 2,
                                outerRadius: outerRadius,
                                startAngle: obj.startAngle,
                                endAngle: obj.endAngle})})`
                            }
                            textAnchor="middle"
                            fill="white"
                            style={{fontSize: 13}}>
                        <tspan style={{fontSize: 10}}>
                        {obj.data + " stocks in " + this.state.pieChartNames[i]}
                        </tspan>
                    </text>
                    )}
                </svg>
                </div>
                </div>
            );
        }
    }
}

export default CustomerPieChart;