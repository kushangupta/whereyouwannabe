import React from 'react';
import Button from 'react-bootstrap/Button'
import axios from 'axios';
import SearchBar from './SearchBar.js';

import {
    Dropdown,
    ButtonToolbar,
    InputGroup,
    FormControl
  } from "react-bootstrap";

class CitiesFilterMenu extends React.Component {
	state = {
	    cities: null,
	    page: 1,
		stateFilterText: "State",
		majorFilterText: "Major",
		apiLink: "fake api link for now",
		SortText: "Sort",
		displayedSortText: "Sort",
		searchQuery: "Search",
		search: "search_cities"
	}

	sortMap = {
        "Name": "city_name",
        "Crime Index": "crime_index",
        "Rent Index": "rent_index",
        "Health Care Index": "health_care_index"
    };

	constructor(props) {
        super(props);
    }

	render() {
		const search = this.state.search;

		return (
		<ButtonToolbar className="CitiesFilter">
			<SearchBar search={search} />
			<Dropdown className='m-1' id='dropdown-btn'>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					{this.state.stateFilterText}
				</Dropdown.Toggle>
				<Dropdown.Menu className='ourdropdown' id='dropdown-options'>
					{["All", "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"].map(value => {
					return (
						<Dropdown.Item
						key={value}
						onClick={() => {
							this.setState({ stateFilterText : value });
						}}
						>
						{value}
						</Dropdown.Item>
					);
					})}
				</Dropdown.Menu>
				</Dropdown>

				<Dropdown className='m-1' id='dropdown-btn'>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					{this.state.majorFilterText}
				</Dropdown.Toggle>
				<Dropdown.Menu className='ourdropdown' id='dropdown-options'>
					{["All", "communication", "engineering", "biological", "computer", "humanities", "health", "psychology", "mathematics"].map(value => {
					return (
						<Dropdown.Item
						key={value}
						onClick={() => {
							this.setState({ majorFilterText : value });
						}}
						>
						{value}
						</Dropdown.Item>
					);
					})}
				</Dropdown.Menu>
				</Dropdown>

				<Dropdown className='m-1' id='dropdown-btn'>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					{this.state.displayedSortText}
				</Dropdown.Toggle>

				<Dropdown.Menu className='ourdropdown' id='dropdown-options'>
					{["All", "Name", "Crime Index", "Rent Index", "Health Care Index"].map(value => {
					return (
						<Dropdown.Item
						key={value}
						onClick={() => {
							this.setState({ SortText : this.sortMap[value], displayedSortText : value});
						}}
						>
						{value}
						</Dropdown.Item>
					);
					})}
				</Dropdown.Menu>
				</Dropdown>

			<Button className='m-1' id='go-btn'
				onClick={() => {
					axios.get(`https://api.whereyouwannabe.org/cities/filter?state=` + this.state.stateFilterText + "&major=" + this.state.majorFilterText + "&sort=" + this.state.SortText)
					.then(res => {
						const cities = res.data;
						this.setState({ cities });
						this.setState({page : 1})
						this.props.setter(`https://api.whereyouwannabe.org/cities/filter?state=` + this.state.stateFilterText + "&major=" + this.state.majorFilterText + "&sort=" + this.state.SortText);
					})
				}}
				>
				Go
			</Button>
		</ButtonToolbar>
		);
	}
}

export default CitiesFilterMenu;
