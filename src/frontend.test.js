import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure} from 'enzyme';	
import Adapter from 'enzyme-adapter-react-16';
import App from './App';
import Navigation from './Components/Navigation'
import GitlabTable from './Components/GitlabTable'
import Cities from './Pages/Cities.js'
import CityInstance from "./Pages/CityInstance";
import ProfessionInstance from "./Pages/ProfessionInstance";
import Professions from "./Pages/Professions";
import Universities from "./Pages/Universities";
import UniversityInstance from "./Pages/UniversityInstance";
import CitiesFilterMenu from "./Components/CitiesFilterMenu.js"
import ProfessionsFilterMenu from "./Components/ProfessionsFilterMenu.js"
import UniversitiesFilterMenu from "./Components/UniversitiesFilterMenu.js"
import BubbleChart from "./Components/BubbleChart.js"
import CitiesBarGraph from "./Components/CitiesBarGraph.js"
import About from './Pages/About'

configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});


describe('<Navigation />', () => {
	it('renders a Navigation Bar', () => {
	  const nav = shallow(<Navigation />);

      expect(nav).toMatchSnapshot();
	  
	});
});

it('renders GitLabTable', async () => {
  const component = shallow(<GitlabTable/>);
  const data = component.instance();
  await data.componentDidMount();
  expect(component).toMatchSnapshot();
	
});

it('Cities', async () => {
  const component = shallow(<Cities/>);
  const data = component.instance();
  await data.componentDidMount();
  expect(component).toMatchSnapshot();	

});

it('CityInstance', async () => {
  const component = shallow(<CityInstance/>);
  const data = component.instance();
  await data.componentDidMount();
  expect(component).toMatchSnapshot();
});

it('Professions', async () => {
  const component = shallow(<Professions/>);
  const data = component.instance();
  await data.componentDidMount();
  expect(component).toMatchSnapshot();

});

it('ProfessionInstance', async () => {
  const component = shallow(<ProfessionInstance/>);
  const data = component.instance();
  await data.componentDidMount();
  expect(component).toMatchSnapshot();
});

it('Universities', async () => {
  const component = shallow(<Universities/>);
  const data = component.instance();
  await data.componentDidMount();
  expect(component).toMatchSnapshot();
});

it('UniversityInstance', async () => {
  const component = shallow(<UniversityInstance/>);
  const data = component.instance();
  await data.componentDidMount();
  expect(component).toMatchSnapshot();
  }
);

it('About', async () => {
  const component = shallow(<About/>);
  expect(component).toMatchSnapshot();
});

it('CitiesFilterMenu', async () => {
  const component = shallow(<CitiesFilterMenu/>);
  expect(component).toMatchSnapshot();
  }
);

it('ProfessionsFilterMenu', async () => {
  const component = shallow(<ProfessionsFilterMenu/>);
  expect(component).toMatchSnapshot();
  }
);

it('UniversitiesFilterMenu', async () => {
  const component = shallow(<UniversitiesFilterMenu/>);
  expect(component).toMatchSnapshot();
  }
);

it('BubbleChart', async () => {
  const component = shallow(<BubbleChart/>);
  expect(component).toMatchSnapshot();
  }
);

it('CitiesBarGraph', async () => {
  const component = shallow(<CitiesBarGraph/>);
  expect(component).toMatchSnapshot();
  }
);