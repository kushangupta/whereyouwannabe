from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from unittest import main, TestCase
import time

class GUITests(TestCase):
	
	@classmethod
	def setUpClass(self):
		self.browser = webdriver.Chrome('/Users/alex/Downloads/chromedriver')
		time.sleep(5)

	@classmethod
	def tearDownClass(self):
		self.browser.close()

	def setUp(self):
		self.browser.get("localhost:3000")
		time.sleep(3)

	def test_navbar_cities(self):
		cities_link = self.browser.find_element_by_link_text('Cities')
		self.assertEqual(cities_link.text, 'Cities')
		cities_link.click()
		expected_text = 'Cities'
		time.sleep(3)
		actual_text = self.browser.find_element_by_class_name('Cities-header').text
		self.assertEqual(expected_text, actual_text)

	def test_navbar_universities(self):
		universities_link = self.browser.find_element_by_link_text('Universities')
		self.assertEqual(universities_link.text, 'Universities')
		universities_link.click()
		expected_text = 'Universities'
		time.sleep(3)
		actual_text = self.browser.find_element_by_class_name('Cities-header').text
		self.assertEqual(expected_text, actual_text)

	def test_navbar_professions(self):
		professions_link = self.browser.find_element_by_link_text('Professions')
		self.assertEqual(professions_link.text, 'Professions')
		professions_link.click()
		expected_text = 'Professions'
		time.sleep(3)
		actual_text = self.browser.find_element_by_class_name('Cities-header').text
		self.assertEqual(expected_text, actual_text)

	def test_navbar_about(self):
		about_link = self.browser.find_element_by_link_text('About')
		self.assertEqual(about_link.text, 'About')
		about_link.click()
		expected_text = 'about us'
		time.sleep(3)
		actual_text = self.browser.find_element_by_class_name('Model-header').text
		self.assertEqual(expected_text, actual_text)

	def test_home_page(self):
		expected_text = 'Start Your Journey to a Better Lifestyle'
		actual_text = self.browser.find_element_by_class_name('Title-summary').text[0 : len(expected_text)]
		self.assertEqual(expected_text, actual_text)

	def test_back_and_forth_pages(self):
		about_link = self.browser.find_element_by_link_text('About')
		self.assertEqual(about_link.text, 'About')
		about_link.click()
		home_link = self.browser.find_element_by_link_text('WhereYouWannaBe')
		self.assertEqual(home_link.text, 'WhereYouWannaBe')
		home_link.click()
		
		expected_text = 'Start Your Journey to a Better Lifestyle'
		time.sleep(3)
		actual_text = self.browser.find_element_by_class_name('Title-summary').text[0 : len(expected_text)]
		self.assertEqual(expected_text, actual_text)

	def test_city_instance(self):
		about_link = self.browser.find_element_by_link_text('Cities')
		self.assertEqual(about_link.text, 'Cities')
		about_link.click()

		time.sleep(3)
		city_instance = self.browser.find_element_by_class_name('city-instance')
		city_instance.click()

		time.sleep(3)
		expected_text = 'Crime Index:'
		self.assertTrue(expected_text in self.browser.page_source)

	def test_profession_instance(self):
		about_link = self.browser.find_element_by_link_text('Professions')
		self.assertEqual(about_link.text, 'Professions')
		about_link.click()

		time.sleep(3)
		city_instance = self.browser.find_element_by_class_name('profession-instance')
		city_instance.click()

		time.sleep(3)
		expected_text = 'Median Wage:'
		self.assertTrue(expected_text in self.browser.page_source)

	def test_university_instance(self):
		about_link = self.browser.find_element_by_link_text('Universities')
		self.assertEqual(about_link.text, 'Universities')
		about_link.click()

		time.sleep(3)
		city_instance = self.browser.find_element_by_class_name('university-instance')
		city_instance.click()

		time.sleep(3)
		expected_text = 'Admission Rate:'
		self.assertTrue(expected_text in self.browser.page_source)

	def test_about_links(self):
		about_link = self.browser.find_element_by_link_text('About')
		self.assertEqual(about_link.text, 'About')
		about_link.click()

		time.sleep(3)
		gitlab_link = self.browser.find_elements_by_class_name("center")
		gitlab_link[1].click()

	def test_cities_filtering(self):
		about_link = self.browser.find_element_by_link_text('Cities')
		self.assertEqual(about_link.text, 'Cities')
		about_link.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//button[text()='State']")
		state_filter.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//a[text()='CA']")
		state_filter.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//button[text()='Go']")
		state_filter.click()

		time.sleep(3)
		expected_text = 'San Diego'
		self.assertTrue(expected_text in self.browser.page_source)

	def test_cities_filtering(self):
		about_link = self.browser.find_element_by_link_text('Professions')
		self.assertEqual(about_link.text, 'Professions')
		about_link.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//button[text()='Education']")
		state_filter.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//a[text()='All']")
		state_filter.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//button[text()='Go']")
		state_filter.click()

		time.sleep(3)
		expected_text = 'Office Machine'
		self.assertTrue(expected_text in self.browser.page_source)

	def test_universities_filtering(self):
		about_link = self.browser.find_element_by_link_text('Universities')
		self.assertEqual(about_link.text, 'Universities')
		about_link.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//button[text()='State']")
		state_filter.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//a[text()='CA']")
		state_filter.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//button[text()='Go']")
		state_filter.click()

		time.sleep(3)
		expected_text = 'Stanford University'
		self.assertTrue(expected_text in self.browser.page_source)

	def test_cities_sorting(self):
		about_link = self.browser.find_element_by_link_text('Cities')
		self.assertEqual(about_link.text, 'Cities')
		about_link.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_id("search-box")
		state_filter.send_keys("Buffalo")

		search_button = self.browser.find_element_by_xpath("//a[text()='Search']")
		search_button.click()

		time.sleep(3)
		expected_text = 'engineering'
		self.assertTrue(expected_text in self.browser.page_source)
	
	def test_professions_sorting(self):
		about_link = self.browser.find_element_by_link_text('Professions')
		self.assertEqual(about_link.text, 'Professions')
		about_link.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//*[@id='search-box']")
		state_filter.send_keys("Computer")

		search_button = self.browser.find_element_by_xpath("//a[text()='Search']")
		search_button.click()

		time.sleep(3)
		professions_button = self.browser.find_element_by_xpath("//button[text()='Professions']")
		professions_button.click()

		time.sleep(3)
		expected_text = 'word processing systems'
		self.assertTrue(expected_text in self.browser.page_source)

	def test_universities_sorting(self):
		about_link = self.browser.find_element_by_link_text('Universities')
		self.assertEqual(about_link.text, 'Universities')
		about_link.click()

		time.sleep(3)
		state_filter = self.browser.find_element_by_xpath("//*[@id='search-box']")
		state_filter.send_keys("Fall")

		search_button = self.browser.find_element_by_xpath("//a[text()='Search']")
		search_button.click()

		time.sleep(3)
		professions_button = self.browser.find_element_by_xpath("//button[text()='Universities']")
		professions_button.click()

		time.sleep(3)
		expected_text = 'Cedar'
		self.assertTrue(expected_text in self.browser.page_source)

		
if __name__ == '__main__':
	main()