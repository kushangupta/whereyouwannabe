import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Cities from './Pages/Cities.js';
import Professions from './Pages/Professions.js';
import About from './Pages/About.js';
import CityInstance from "./Pages/CityInstance.js";
import ProfessionInstance from "./Pages/ProfessionInstance.js";
import Universities from './Pages/Universities.js';
import UniversityInstance from './Pages/UniversityInstance.js';
import Api from './Pages/Api.js';
import Search from './Pages/Search.js'
import Visualizations from './Pages/Visualizations.js'

ReactDOM.render(
	<Router>
    <Switch>
      <Route exact path='/' render={() => <App />} />
      <Route exact path='/cities' render={() => <Cities />} />
      <Route path={'/cities/:city'} render={() => <CityInstance />} />
      <Route path={'/professions/:professions'} render={() => <ProfessionInstance />} />
      <Route exact path='/professions' render={() => <Professions />} />
      <Route exact path='/universities' render={() => <Universities />} />
      <Route path={'/universities/:uni'} render={() => <UniversityInstance />} />
      <Route exact path='/about' render={() => <About />} />
      <Route exact path='/api' render={() => <Api />}/>
      <Route path={'/search/:model/:query'} render={() => <Search />} />
      <Route exact path={'/visualizations'} render={() => <Visualizations />}/>
    </Switch>
   </Router>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
