<pre>
Group members:

Name            eid             gitlab id       estimated completion time     actual completion time
----------------------------------------------------------------------------------------------------
Bryce Dai       bd23626         brycedai                10 hours                      15 hours
Lisa Yu         ly4465          lisayu                  28 hours                      22 hours
Karen Ding      kld2779         kayrhen                 30 hours                      20 hours
Kushan Gupta    kg28868         kushangupta             15 hours                      18 hours
Alexander Zhao  awz222          azhao6                  20 hours                      25 hours

Git SHA: e10e6bad4ff93186f701754e8798d192535eaf41

Project Leader: Alexander Zhao

Gitlab Pipelines: https://gitlab.com/kushangupta/whereyouwannabe/pipelines

Website Link: https://www.whereyouwannabe.org

Comments: This phase was fairly smooth because it mostly involved clean up and not creating new
features aside from the visualizations. We also employed a partitioning of teams to work on each
visualization which was a very successful way to divide and conquer our work. Refactoring was
not too bad since our code was written with a scalable design in mind.
</pre>